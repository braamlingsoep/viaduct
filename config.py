import logging

import pydantic

_logger = logging.getLogger(__name__)


class SqlalchemyPostgresDsn(pydantic.PostgresDsn):
    allowed_schemes = {"postgres+psycopg2", "postgresql+psycopg2"}


class ConnectionTestUrls(pydantic.BaseSettings):
    # ignore type: https://github.com/samuelcolvin/pydantic/issues/1490
    sqlalchemy_database_test_uri: SqlalchemyPostgresDsn = "postgresql+psycopg2://viaduct:viaduct@localhost:5433/viaduct_test"  # type: ignore[assignment]  # noqa: E501
    broker_test_url: pydantic.RedisDsn = "redis://redis"  # type: ignore[assignment]
