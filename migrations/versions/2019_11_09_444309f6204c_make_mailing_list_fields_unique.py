"""Make mailing list fields unique.

Revision ID: 444309f6204c
Revises: 4a1c0b9e91db
Create Date: 2019-11-09 15:31:26.729016

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "444309f6204c"
down_revision = "4a1c0b9e91db"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint(
        op.f("uq_mailing_list_copernica_column_name"),
        "mailing_list",
        ["copernica_column_name"],
    )
    op.create_unique_constraint(
        op.f("uq_mailing_list_en_name"), "mailing_list", ["en_name"]
    )
    op.create_unique_constraint(
        op.f("uq_mailing_list_nl_name"), "mailing_list", ["nl_name"]
    )
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(op.f("uq_mailing_list_nl_name"), "mailing_list", type_="unique")
    op.drop_constraint(op.f("uq_mailing_list_en_name"), "mailing_list", type_="unique")
    op.drop_constraint(
        op.f("uq_mailing_list_copernica_column_name"), "mailing_list", type_="unique"
    )
    # ### end Alembic commands ###


# vim: ft=python
