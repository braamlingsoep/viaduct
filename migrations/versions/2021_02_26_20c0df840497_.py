"""empty message.

Revision ID: 20c0df840497
Revises: ('ef7b23cf0e6b', 'ac1aa04899fe')
Create Date: 2021-02-26 10:40:37.648296

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "20c0df840497"
down_revision = ("ef7b23cf0e6b", "ac1aa04899fe")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()


def downgrade():
    create_session()


# vim: ft=python
