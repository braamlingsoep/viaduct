"""Change activity.price maxlen to 256

Revision ID: f232406dc738
Revises: 370cd2ee5993
Create Date: 2021-11-13 14:45:28.313811

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "f232406dc738"
down_revision = "e514f7447b29"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    op.alter_column(
        "activity",
        "price",
        existing_type=sa.VARCHAR(length=16),
        type_=sa.VARCHAR(length=256),
        existing_nullable=False,
    )


def downgrade():
    create_session()

    op.alter_column(
        "activity",
        "price",
        existing_type=sa.VARCHAR(length=256),
        type_=sa.VARCHAR(length=16),
        existing_nullable=False,
    )


# vim: ft=python
