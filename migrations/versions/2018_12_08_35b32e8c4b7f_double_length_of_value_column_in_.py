"""Double length of value column in settings table.

Revision ID: 35b32e8c4b7f
Revises: b7a9f09d5625
Create Date: 2018-12-08 12:24:40.710085

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "35b32e8c4b7f"
down_revision = "b7a9f09d5625"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "setting",
        "value",
        existing_type=sa.VARCHAR(length=128),
        type_=sa.String(length=256),
        existing_nullable=True,
    )
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "setting",
        "value",
        existing_type=sa.String(length=256),
        type_=sa.VARCHAR(length=128),
        existing_nullable=True,
    )
    # ### end Alembic commands ###


# vim: ft=python
