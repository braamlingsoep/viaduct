"""Convert honary_member to memer_of_merit_date

Revision ID: 56a6c488a084
Revises: c0b53dc0219e
Create Date: 2021-04-21 15:14:32.112100

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import Column, Integer, Boolean, Date, update
from sqlalchemy.sql import functions as func
from sqlalchemy.dialects import postgresql

from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "56a6c488a084"
down_revision = "c0b53dc0219e"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


class User(db.Model):
    __tablename__ = "user"
    id = Column(Integer(), primary_key=True)

    member_of_merit_date = Column(Date, nullable=True)
    honorary_member: bool = Column(Boolean, nullable=False, default=False)


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("user", sa.Column("member_of_merit_date", sa.Date(), nullable=True))
    stmt = (
        update(User)
        .where(User.honorary_member.is_(True))
        .values({User.member_of_merit_date: func.now()})
    )
    print(stmt)
    db.session.execute(stmt)
    op.drop_column("user", "honorary_member")
    db.session.commit()
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "user",
        sa.Column("honorary_member", sa.BOOLEAN(), autoincrement=False, nullable=True),
    )
    stmt = update(User).values(
        {User.honorary_member: User.member_of_merit_date.isnot(None)}
    )
    db.session.execute(stmt)
    op.drop_column("user", "member_of_merit_date")

    # ### end Alembic commands ###


# vim: ft=python
