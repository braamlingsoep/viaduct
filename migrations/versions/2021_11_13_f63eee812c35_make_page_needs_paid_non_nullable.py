"""Make page needs_paid non-nullable

Revision ID: f63eee812c35
Revises: 370cd2ee5993
Create Date: 2021-11-13 13:48:03.168075

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "f63eee812c35"
down_revision = "370cd2ee5993"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    op.alter_column("page", "needs_paid", existing_type=sa.BOOLEAN(), nullable=False)


def downgrade():
    create_session()

    op.alter_column("page", "needs_paid", existing_type=sa.BOOLEAN(), nullable=True)


# vim: ft=python
