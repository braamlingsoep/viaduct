"""settings_photo.

Revision ID: b763ff436a2e
Revises: 35b32e8c4b7f
Create Date: 2019-02-09 10:50:46.560129

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "b763ff436a2e"
down_revision = "35b32e8c4b7f"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship

keys = [
    "FLICKR_USER",
    "FLICKR_API_KEY",
    "FLICKR_SECRET",
    "FLICKR_TOKEN",
    "FLICKR_TOKEN_SECRET",
]

settings_table = db.table(
    "setting",
    db.Column("key", db.String(128), unique=True, nullable=False),
    db.Column("value", db.String(256)),
)


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    op.bulk_insert(
        settings_table,
        [
            {"key": "FLICKR_USER", "value": "150227107@N08"},
            {"key": "FLICKR_API_KEY", "value": "dummy"},
            {"key": "FLICKR_SECRET", "value": "dummy"},
            {"key": "FLICKR_TOKEN", "value": "dummy"},
            {"key": "FLICKR_TOKEN_SECRET", "value": "dummy"},
        ],
    )


def downgrade():
    create_session()


# vim: ft=python
