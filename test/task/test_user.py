from datetime import datetime, timedelta, timezone

import pytest
from sqlalchemy import insert, select

from app.models.education import Education
from app.models.setting_model import Setting
from app.models.user import UserEducation, UserEducationStatus
from app.task.user import update_most_historic_user_educations


@pytest.fixture(params=[True, False])
def disable_setting(db_session, request):
    if request.param:
        stmt = insert(Setting).values(
            {Setting.key: "user_education_updates_enabled", Setting.value: "False"}
        )
        db_session.execute(stmt)
        db_session.commit()
    return request.param


def test_update_most_historic_user_educations(
    db_session, user_factory, education_factory, requests_mocker, disable_setting
):
    requests_mocker.get(
        "https://api.datanose.nl/Programmes/123456789",
        json=[
            {
                "Code": "MSc PhA",
                "Name": "Master Physics and Astronomy (joint degree)",
                "Type": "Master",
            }
        ],
    )
    user = user_factory(student_id="123456789")
    education = education_factory(datanose_code="MSc PhA")

    two_months_ago = datetime.now() - timedelta(days=60)
    user_education = UserEducation(
        created=two_months_ago,
        modified=two_months_ago,
        user_id=user.id,
        education_id=education.id,
    )
    db_session.add(user_education)
    db_session.commit()

    # Call without .delay() to skip broker.
    update_most_historic_user_educations()

    db_session.refresh(user_education)

    if disable_setting:
        assert not requests_mocker.called_once
    else:
        s = db_session.scalar(
            select(UserEducationStatus).where(UserEducationStatus.user_id == user.id)
        )
        assert s.last_checked - datetime.now(timezone.utc) < timedelta(hours=1)
        assert user_education.last_seen is None
        assert requests_mocker.called_once


def test_update_most_historic_user_educations_graduated(
    db_session, user_factory, education_factory, requests_mocker, disable_setting
):
    requests_mocker.get(
        "https://api.datanose.nl/Programmes/123456789",
        json=[],
    )
    user = user_factory(student_id="123456789")
    education = education_factory(datanose_code="MSc PhA")

    two_months_ago = datetime.now(timezone.utc) - timedelta(days=60)
    user_education = UserEducation(
        created=two_months_ago,
        modified=two_months_ago,
        user_id=user.id,
        education_id=education.id,
    )
    db_session.add(user_education)
    db_session.commit()

    # Call without .delay() to skip broker.
    update_most_historic_user_educations()

    db_session.refresh(user_education)

    if disable_setting:
        assert not requests_mocker.called_once
    else:
        s = db_session.scalar(
            select(UserEducationStatus).where(UserEducationStatus.user_id == user.id)
        )
        assert s.last_checked - datetime.now(timezone.utc) < timedelta(hours=1)
        assert user_education.last_seen is not None
        assert requests_mocker.called_once


def test_update_most_historic_user_unconfirmed_educations(
    db_session, user_factory, education_factory, requests_mocker
):
    user = user_factory(student_id_confirmed=False)
    education = education_factory()

    two_months_ago = datetime.now(timezone.utc) - timedelta(days=60)
    user_education = UserEducation(
        created=two_months_ago,
        modified=two_months_ago,
        user_id=user.id,
        education_id=education.id,
    )
    db_session.add(user_education)
    db_session.commit()

    # Call without .delay() to skip broker.
    update_most_historic_user_educations()

    db_session.refresh(user_education)

    assert not requests_mocker.called_once
    assert user_education.modified == two_months_ago
    assert not db_session.scalar(select(UserEducationStatus))


def test_update_most_historic_user_educations_no_current_education(
    user_factory, db_session, education_factory, requests_mocker
):
    requests_mocker.get(
        "https://api.datanose.nl/Programmes/123456789",
        json=[
            {
                "Code": "MSc PhA",
                "Name": "Master Physics and Astronomy (joint degree)",
                "Type": "Master",
            }
        ],
    )
    user = user_factory(student_id="123456789")
    education_factory(datanose_code="MSc PhA")

    # Call without .delay() to skip broker.
    update_most_historic_user_educations()

    assert requests_mocker.called_once
    s = db_session.scalar(
        select(UserEducationStatus).where(UserEducationStatus.user_id == user.id)
    )
    assert s.last_checked - datetime.now(timezone.utc) < timedelta(minutes=1)


def test_update_most_historic_user_educations_unknown_education(
    user_factory, db_session, education_factory, requests_mocker, caplog
):
    stmt = select(Education).filter(Education.datanose_code == "MSc BMS")
    assert not db_session.scalar(stmt)
    requests_mocker.get(
        "https://api.datanose.nl/Programmes/123456789",
        json=[
            {
                "Code": "MSc BMS",
                "Name": "Master Biomedical Sciences",
                "Type": "Master",
            },
            {
                "Code": "MSc PhA",
                "Name": "Master Physics and Astronomy (joint degree)",
                "Type": "Master",
            },
        ],
    )
    user = user_factory(student_id="123456789")
    assert user.educations.all() == []
    education = education_factory(datanose_code="MSc PhA")

    # Call without .delay() to skip broker.
    update_most_historic_user_educations()

    assert requests_mocker.called_once
    assert user.educations.all() == [education]
    s = db_session.scalar(
        select(UserEducationStatus).where(UserEducationStatus.user_id == user.id)
    )
    assert s.last_checked - datetime.now(timezone.utc) < timedelta(minutes=1)
