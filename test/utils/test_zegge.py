from app.utils.zegge import zegge


def test_below_20():
    res = [
        "nul",
        "een",
        "twee",
        "drie",
        "vier",
        "vijf",
        "zes",
        "zeven",
        "acht",
        "negen",
        "tien",
        "elf",
        "twaalf",
        "dertien",
        "veertien",
        "vijftien",
        "zestien",
        "zeventien",
        "achttien",
        "negentien",
        "twintig",
    ]
    assert [res[i] == zegge(i) for i in range(21)]


def test_thousands():
    assert zegge(1000) == "duizend"
    assert zegge(2000) == "tweeduizend"
    assert zegge(3000) == "drieduizend"
    assert zegge(13000) == "dertienduizend"
    assert zegge(31000) == "eenendertigduizend"


def test_hundreds():
    assert zegge(1100) == "elfhonderd"
    assert zegge(2300) == "drieëntwintighonderd"
    assert zegge(6100) == "eenenzestighonderd"


def test_random_numbers():
    assert zegge(7224) == "zevenduizendtweehonderdvierentwintig"
    assert zegge(2618) == "tweeduizendzeshonderdachttien"
    assert zegge(1020) == "duizendtwintig"
    assert zegge(3730) == "drieduizendzevenhonderddertig"
    assert zegge(3451) == "drieduizendvierhonderdeenenvijftig"
