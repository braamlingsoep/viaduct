from sqlalchemy.orm import Session

from app.models.page import Page, PageRevision
from app.models.redirect import Redirect
from conftest import CustomClient


def test_page_404(admin_client):
    path = "/pagepathwhichisnotyetcreated"
    rv = admin_client.get(path)
    assert rv.status_code == 404
    assert "text/html" in rv.content_type


def test_page(admin_client, page_revision: tuple[Page, PageRevision]):
    page, revision = page_revision
    # Page has now been created
    rv = admin_client.get(page.path)
    assert rv.status_code == 200


def test_page_redirect_exist(
    admin_client: CustomClient,
    page_revision: tuple[Page, PageRevision],
    db_session: Session,
) -> None:
    page, _ = page_revision
    r = Redirect(fro=page.path, to="https://example.com")
    db_session.add(r)
    db_session.commit()

    # Page takes priority over redirect.
    rv = admin_client.get(page.path)
    assert rv.status_code == 200


def test_create_edit(
    anonymous_client, admin_user, page_revision: tuple[Page, PageRevision]
):
    page, _ = page_revision
    anonymous_client.login(admin_user)

    rv = anonymous_client.get("/pages/create/")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/pages/{page.id}/edit/")
    assert rv.status_code == 200
