import json
from urllib.parse import parse_qs, urlparse

import pytest
from sqlalchemy.orm import Session

from app.models.oauth.client import OAuthClient
from app.models.oauth.token import OAuthToken
from conftest import CustomClient


def test_oauth_authorize_authorization_code(
    anonymous_client, member_user, oauth_client: OAuthClient
):
    anonymous_client.login(member_user)

    rv = anonymous_client.get(
        "/oauth/authorize",
        query_string={
            "response_type": "code",
            "client_id": oauth_client.client_id,
            "redirect_uri": oauth_client.redirect_uris[0],
            "scope": " ".join(oauth_client.scopes),
            "state": "somerandomstate",
        },
        # Add specific HTTPS, as AUTHLIB_INSECURE_TRANSPORT is not set during test
        url_scheme="https",
    )
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/oauth/authorize",
        data={
            "client_id": oauth_client.client_id,
            "scope": " ".join(oauth_client.scopes),
            "response_type": "code",
            "redirect_uri": oauth_client.redirect_uris[0],
            "state": "somerandomstate",
            "confirm": "Accept",
        },
        # Add specific HTTPS, as AUTHLIB_INSECURE_TRANSPORT is not set during test
        url_scheme="https",
        follow_redirects=False,
    )
    assert rv.status_code == 302
    location = urlparse(rv.location)
    querystring = parse_qs(location.query)
    assert "code" in querystring
    assert querystring["state"] == ["somerandomstate"]

    rv = anonymous_client.post(
        "/oauth/token",
        data={
            "code": querystring["code"],
            "client_id": oauth_client.client_id,
            "client_secret": oauth_client.client_secret,
            "redirect_uri": oauth_client.redirect_uris[0],
            "grant_type": "authorization_code",
            "scope": " ".join(oauth_client.scopes),
        },
        # Add specific HTTPS, as AUTHLIB_INSECURE_TRANSPORT is not set during test
        url_scheme="https",
    )
    assert rv.status_code == 200, rv.data
    data = json.loads(rv.data)
    assert "access_token" in data
    assert "refresh_token" in data

    rv = anonymous_client.post(
        "/oauth/token",
        data={
            "client_id": oauth_client.client_id,
            "client_secret": oauth_client.client_secret,
            "refresh_token": rv.json["refresh_token"],
            "grant_type": "refresh_token",
            "scope": " ".join(oauth_client.scopes),
        },
        # Add specific HTTPS, as AUTHLIB_INSECURE_TRANSPORT is not set during test
        url_scheme="https",
    )
    assert rv.status_code == 200, rv.json
    assert "access_token" in rv.json


@pytest.mark.parametrize("token_type_hint", ["access_token", None])
def test_revoke_access_token(
    member_client: CustomClient,
    member_token: OAuthToken,
    oauth_client: OAuthClient,
    token_type_hint: str | None,
    db_session: Session,
) -> None:
    rv = member_client.post(
        "/oauth/revoke",
        data={
            "client_id": oauth_client.client_id,
            "client_secret": oauth_client.client_secret,
            "token_type_hint": token_type_hint,
            "token": member_token.access_token,
        },
        url_scheme="https",
    )
    assert rv.status_code == 200, rv.json

    rv = member_client.post(
        "/oauth/introspect",
        data={
            "token": member_token.access_token,
            "client_id": oauth_client.client_id,
            "client_secret": oauth_client.client_secret
            # Add base URL, because we require token introspection to be on https.
        },
        url_scheme="https",
    )
    assert rv.status_code == 200, rv.json
    assert rv.json["active"] is False, rv.json
    db_session.refresh(member_token)
    assert member_token.is_revoked()


@pytest.mark.parametrize("token_type_hint", ["refresh_token", None])
def test_revoke_refresh_token(
    member_client: CustomClient,
    member_token: OAuthToken,
    oauth_client: OAuthClient,
    token_type_hint: str | None,
    db_session: Session,
) -> None:
    rv = member_client.post(
        "/oauth/revoke",
        data={
            "client_id": oauth_client.client_id,
            "client_secret": oauth_client.client_secret,
            "token_type_hint": token_type_hint,
            "token": member_token.refresh_token,
        },
        url_scheme="https",
    )
    assert rv.status_code == 200, rv.json

    rv = member_client.post(
        "/oauth/introspect",
        data={
            "token": member_token.access_token,
            "client_id": oauth_client.client_id,
            "client_secret": oauth_client.client_secret
            # Add base URL, because we require token introspection to be on https.
        },
        url_scheme="https",
    )
    assert rv.status_code == 200, rv.json
    assert rv.json["active"] is False, rv.json
    db_session.refresh(member_token)
    assert member_token.is_revoked()
