def test_minutes_raw(minute_factory, admin_user, admin_client):
    admin_client.login(admin_user)
    minute = minute_factory()

    rv = admin_client.get(f"/pimpy/minutes/single/{minute.id}/raw")
    assert rv.status_code == 200
    assert "text/plain" in rv.content_type


def test_vue_pages(admin_user, anonymous_client, admin_group, minute_factory):
    anonymous_client.login(admin_user)

    rv = anonymous_client.get(f"/pimpy/tasks/?group={admin_group.id}")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/pimpy/minutes/?group={admin_group.id}")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/pimpy/tasks/create/?group={admin_group.id}")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/pimpy/minutes/create/?group={admin_group.id}")
    assert rv.status_code == 200

    minute = minute_factory()
    rv = anonymous_client.get(f"/pimpy/minutes/single/{minute.id}")
    assert rv.status_code == 200
