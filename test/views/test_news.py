from collections.abc import Callable
from datetime import date

from app.models.news import News
from conftest import CustomClient


def test_create_edit(anonymous_client, admin_user, news_factory):
    news_item = news_factory()
    anonymous_client.login(admin_user)

    rv = anonymous_client.get("/news/create/")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/news/{news_item.id}/edit/")
    assert rv.status_code == 200


def test_future_news(
    anonymous_client, admin_client, news_factory, db_session, member_user
):
    news_item_today = news_factory()
    news_item_future = news_factory()
    date_today = date.today()
    news_item_today.publish_date = date_today
    date_future = date(date_today.year + 1, date_today.month, date_today.day)
    news_item_future.publish_date = date_future
    db_session.commit()

    rv = anonymous_client.get(
        f"news/{news_item_today.id}/view/", follow_redirects=False
    )
    assert rv.status_code == 200

    rv = anonymous_client.get(
        f"news/{news_item_future.id}/view/", follow_redirects=False
    )
    assert rv.status_code == 302

    anonymous_client.login(member_user)

    rv = anonymous_client.get(
        f"news/{news_item_future.id}/view/", follow_redirects=False
    )
    assert rv.status_code == 403

    rv = admin_client.get(f"news/{news_item_today.id}/view/", follow_redirects=False)
    assert rv.status_code == 200

    rv = admin_client.get(f"news/{news_item_future.id}/view/", follow_redirects=False)
    assert rv.status_code == 200


def test_news_slugs(
    anonymous_client, admin_client, news_factory, db_session, member_user
):
    news_item_today = news_factory()
    news_item_future = news_factory()
    date_today = date.today()
    news_item_today.publish_date = date_today
    date_future = date(date_today.year + 1, date_today.month, date_today.day)
    news_item_future.publish_date = date_future
    db_session.commit()

    rv = anonymous_client.get(
        f"news/{news_item_today.id}/{news_item_today.to_slug}/view/",
        follow_redirects=False,
    )
    assert rv.status_code == 200

    rv = anonymous_client.get(
        f"news/{news_item_future.id}/{news_item_today.to_slug}/view/",
        follow_redirects=False,
    )
    assert rv.status_code == 302

    anonymous_client.login(member_user)

    rv = anonymous_client.get(
        f"news/{news_item_future.id}/{news_item_future.to_slug}/view/",
        follow_redirects=False,
    )
    assert rv.status_code == 403

    rv = admin_client.get(
        f"news/{news_item_today.id}/{news_item_today.to_slug}/view/",
        follow_redirects=False,
    )
    assert rv.status_code == 200

    rv = admin_client.get(
        f"news/{news_item_future.id}/{news_item_today.to_slug}/view/",
        follow_redirects=False,
    )
    assert rv.status_code == 200


def test_news_rss(
    anonymous_client: CustomClient, news_factory: Callable[[], News]
) -> None:
    news_factory()
    rv = anonymous_client.get("/news/rss/")
    assert rv.status_code == 200
