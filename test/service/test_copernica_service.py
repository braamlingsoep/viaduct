import unittest
from datetime import datetime
from unittest import mock
from unittest.mock import patch

from app.enums import ProgrammeType
from app.models.education import Education
from app.models.mailinglist_model import MailingList
from app.models.user import User
from app.service import copernica_service, mailinglist_service

user = mock.NonCallableMock(spec=User.__table__.columns.keys())
education1 = mock.NonCallableMock(spec=Education.__table__.columns.keys())
education1.configure_mock(
    id=1,
    nl_name="Artificial Intelligence",
    en_name="Artificial Intelligence",
    programme_type=ProgrammeType.MASTER,
)
education2 = mock.NonCallableMock(spec=Education.__table__.columns.keys())
education2.configure_mock(
    id=2,
    nl_name="Informatica",
    en_name="Computer science",
    programme_type=ProgrammeType.BACHELOR,
)

mailinglist1 = mock.NonCallableMock(spec=MailingList.__table__.columns.keys())
mailinglist1.configure_mock(
    id=1,
    nl_name="Nieuwsbrief",
    en_name="Newsletter",
    copernica_column_name="Ingeschreven",
    members_only=False,
)
mailinglist2 = mock.NonCallableMock(spec=MailingList.__table__.columns.keys())
mailinglist2.configure_mock(
    id=2,
    nl_name="Bedrijfsinformatie",
    en_name="Company information",
    copernica_column_name="Bedrijfsinformatie",
    members_only=True,
)
mailinglist3 = mock.NonCallableMock(spec=MailingList.__table__.columns.keys())
mailinglist3.configure_mock(
    id=3,
    nl_name="Nog een mailinglijst",
    en_name="Another mailinglist",
    copernica_column_name="NogEenMailinglijst",
    members_only=True,
)

mailinglist_service_mock = mock.MagicMock(mailinglist_service)
mailinglist_service_mock.get_all_mailinglists.return_value = [
    mailinglist1,
    mailinglist2,
    mailinglist3,
]

mailinglists_property_mock = mock.MagicMock()


@patch.object(copernica_service, "mailinglist_service", mailinglist_service_mock)
class TestCopernicaService(unittest.TestCase):
    def setUp(self) -> None:
        user.configure_mock(
            email="john.doe@svia.nl",
            first_name="john",
            last_name="doe",
            student_id="1234567890",
            has_paid=True,
            alumnus=True,
            favourer=True,
            birth_date=datetime(2019, 1, 1),
            mailinglists=mailinglists_property_mock,
            educations=[education1, education2],
            id=1337,
            copernica_id=12345,
            locale="nl",
        )

        mailinglists_property_mock.reset_mock()
        mailinglists_property_mock.all.return_value = [mailinglist1, mailinglist2]

    def test_copernica_dict_for_user(self):
        data = copernica_service.copernica_dict_for_user(user)
        self.assertDictEqual(
            data,
            {
                "Emailadres": "john.doe@svia.nl",
                "Voornaam": "john",
                "Achternaam": "doe",
                "Studie": "MSc Artificial Intelligence",
                "Studienummer": "1234567890",
                "Lid": "Ja",
                "Alumnus": "Ja",
                "VVV": "Ja",
                "Ingeschreven": "Ja",
                "Bedrijfsinformatie": "Ja",
                "NogEenMailinglijst": "Nee",
                "Geboortedatum": "2019-01-01",
                "WebsiteID": "1337",
                "Taal": "nl",
            },
        )

    def test_copernica_dict_for_user_no_member(self):
        user.configure_mock(
            has_paid=False, alumnus=False, favourer=False, educations=[education2]
        )
        data = copernica_service.copernica_dict_for_user(user)
        self.assertDictEqual(
            data,
            {
                "Emailadres": "john.doe@svia.nl",
                "Voornaam": "john",
                "Achternaam": "doe",
                "Studie": "BSc Computer science",
                "Studienummer": "1234567890",
                "Lid": "Nee",
                "Alumnus": "Nee",
                "VVV": "Nee",
                "Ingeschreven": "Ja",
                "Bedrijfsinformatie": "Nee",
                "NogEenMailinglijst": "Nee",
                "Geboortedatum": "2019-01-01",
                "WebsiteID": "1337",
                "Taal": "nl",
            },
        )

    def test_copernica_dict_for_user_no_birth_day(self):
        self.maxDiff = 100900
        user.configure_mock(birth_date=None)
        data = copernica_service.copernica_dict_for_user(user)
        self.assertDictEqual(
            data,
            {
                "Emailadres": "john.doe@svia.nl",
                "Voornaam": "john",
                "Achternaam": "doe",
                "Studie": "MSc Artificial Intelligence",
                "Studienummer": "1234567890",
                "Lid": "Ja",
                "Alumnus": "Ja",
                "VVV": "Ja",
                "Ingeschreven": "Ja",
                "Bedrijfsinformatie": "Ja",
                "NogEenMailinglijst": "Nee",
                "Geboortedatum": "0000-00-00",
                "WebsiteID": "1337",
                "Taal": "nl",
            },
        )

    def test_copernica_dict_for_user_no_programmes(self):
        user.configure_mock(
            has_paid=False, alumnus=False, favourer=False, educations=[]
        )
        data = copernica_service.copernica_dict_for_user(user)
        self.assertDictEqual(
            data,
            {
                "Emailadres": "john.doe@svia.nl",
                "Voornaam": "john",
                "Achternaam": "doe",
                "Studie": "Other",
                "Studienummer": "1234567890",
                "Lid": "Nee",
                "Alumnus": "Nee",
                "VVV": "Nee",
                "Ingeschreven": "Ja",
                "Bedrijfsinformatie": "Nee",
                "NogEenMailinglijst": "Nee",
                "Geboortedatum": "2019-01-01",
                "WebsiteID": "1337",
                "Taal": "nl",
            },
        )
