import pytest

from app.models.newsletter import Newsletter, NewsletterActivity, NewsletterNewsItem


@pytest.fixture
def newsletter(db_session, activity_factory, news_factory):
    activity = activity_factory()
    activity.nl_description = activity.en_description = """
**via** {lang}

[svia.nl](https://svia.nl)

![img](https://svia.nl/favicon.ico)
    """.strip()
    activity.nl_description = activity.nl_description.format(lang="nl")
    activity.en_description = activity.en_description.format(lang="en")
    activity2 = activity_factory()
    activity2.nl_description = activity2.en_description = "OVERWRITTEN"

    news_item = news_factory()
    news_item.nl_content = news_item.en_content = """
**via** {lang}

[svia.nl](https://svia.nl)

![img](https://svia.nl/favicon.ico)
    """.strip()
    news_item.nl_content = news_item.nl_content.format(lang="nl")
    news_item.en_content = news_item.en_content.format(lang="en")

    news_item2 = news_factory()
    news_item2.nl_content = news_item2.en_content = "OVERWRITTEN"
    newsletter = Newsletter()
    newsletter.activities.append(NewsletterActivity(activity=activity))
    newsletter.news_items.append(NewsletterNewsItem(news_item=news_item))
    newsletter.activities.append(
        NewsletterActivity(
            activity=activity2,
            nl_description="OVERWRITE ACTIVITY nl",
            en_description="OVERWRITE ACTIVITY en",
        )
    )
    newsletter.news_items.append(
        NewsletterNewsItem(
            news_item=news_item2,
            nl_description="OVERWRITE NEWS nl",
            en_description="OVERWRITE NEWS en",
        )
    )
    db_session.add(newsletter)
    db_session.commit()
    return newsletter
