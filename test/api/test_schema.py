import pydantic
import pytest

from app.api.schema import StudentIdStr, schema_registry


@pytest.mark.parametrize("schema", ["BugReportRequest", "ChallengeAdminSubmission"])
def test_schema_api(anonymous_client, schema):
    rv = anonymous_client.get(f"/api/_schema/{schema}")
    assert rv.status_code == 200, rv.json


def test_duplicate_register():
    with pytest.raises(ValueError):

        @schema_registry.register
        @schema_registry.register
        class _A(pydantic.BaseModel):
            pass


def test_wrong_query_parameter(admin_client):
    # Unknown query parameters are silently ignored.
    rv = admin_client.get(
        "/api/activities/",
        query_string={"unknown": "unknown"},
    )
    assert rv.status_code == 200, rv.json


def test_constrained_query_parameter(admin_client):
    rv = admin_client.get(
        "/api/activities/",
        query_string={"limit": "1000"},
    )
    assert rv.status_code == 400, rv.json
    assert rv.json["data"]["errors"] == [
        {
            "loc": ["limit"],
            "msg": "ensure this value is less than or equal to 50",
            "type": "value_error.number.not_le",
            "ctx": {"limit_value": 50},
        }
    ], rv.json


@pytest.mark.parametrize(
    "student_id", [10542590, 11056851, 11302968, 11254483, 11030844]
)
def test_pydantic_student_id_number(student_id):
    class Schema(pydantic.BaseModel):
        student_id: StudentIdStr

    data = Schema(student_id=student_id)
    assert data.student_id == student_id


@pytest.mark.parametrize(
    "student_id", [10542591, 11056852, 11302969, 11254484, 11030845]
)
def test_pydantic_student_id_number_invalid(student_id):
    class Schema(pydantic.BaseModel):
        student_id: StudentIdStr

    with pytest.raises(pydantic.ValidationError):
        Schema(student_id=student_id)
