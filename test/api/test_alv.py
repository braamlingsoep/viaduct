from collections.abc import Callable
from datetime import date

import pytest
from sqlalchemy import func, select
from sqlalchemy.orm import Session
from werkzeug.datastructures import FileStorage

from app.models.alv_model import Alv
from conftest import CustomClient


def test_alvs(db_session: Session, admin_client: CustomClient, alv_factory):
    alv_factory()
    assert db_session.scalar(select(func.count(Alv.id))) == 1
    rv = admin_client.get("/api/alvs/")
    assert rv.status_code == 200, rv.json


def test_create_edit(admin_client, admin_user, activity_factory):
    activity = activity_factory()

    rv = admin_client.post(
        "/api/alvs/",
        json={
            "name": {"nl": "nl_name", "en": "en_name"},
            "date": date.today().isoformat(),
            "activity_id": activity.id,
            "chairman_user_id": admin_user.id,
            "secretary_user_id": admin_user.id,
        },
    )
    assert rv.status_code == 201, rv.json

    rv = admin_client.get(f"/api/alvs/{rv.json['id']}/")
    assert rv.status_code == 200, rv.json

    rv = admin_client.patch(
        f"/api/alvs/{rv.json['id']}/",
        json={
            "name": {"nl": "nl_name2", "en": "en_name2"},
            "date": date.today().isoformat(),
            "activity_id": activity.id,
            "chairman_user_id": admin_user.id,
            "secretary_user_id": admin_user.id,
        },
    )
    assert rv.status_code == 200, rv.json


def test_create_edit_empty(admin_client, admin_user, activity_factory):
    activity = activity_factory()

    rv = admin_client.post(
        "/api/alvs/",
        json={
            "name": {"nl": "nl_name", "en": "en_name"},
            "date": date.today().isoformat(),
        },
    )
    assert rv.status_code == 201, rv.json
    assert rv.json["secretary"] is None

    rv = admin_client.get(f"/api/alvs/{rv.json['id']}/")
    assert rv.status_code == 200, rv.json

    #  Adds activity and chairman
    rv = admin_client.patch(
        f"/api/alvs/{rv.json['id']}/",
        json={
            "name": {"nl": "nl_name2", "en": "en_name2"},
            "date": date.today().isoformat(),
            "activity_id": activity.id,
            "chairman_user_id": admin_user.id,
        },
    )
    assert rv.status_code == 200, rv.json

    #  Change other field while keeping chairman
    rv = admin_client.patch(
        f"/api/alvs/{rv.json['id']}/",
        json={
            "name": {"nl": "nl_name2", "en": "en_name2"},
            "date": date.today().isoformat(),
            "activity_id": activity.id,
            "minutes_accepted": True,
        },
    )
    assert rv.status_code == 200, rv.json
    assert rv.json["chairman"] is not None
    assert rv.json["minutes_accepted"]


@pytest.fixture
def minutes_archive_file():
    with open("./test/fixtures/alv/minute_template.zip", "rb") as f:
        yield FileStorage(f, "minute_template.zip")


def test_upload_minutes_archive(
    admin_client: CustomClient,
    minutes_archive_file: FileStorage,
    alv_factory: Callable[[], Alv],
):
    rv = admin_client.post(
        "/api/alvs/parse_minutes/", data={"file": minutes_archive_file}
    )
    assert rv.status_code == 200, rv.json
    parsed_data = rv.json

    alv = alv_factory()
    rv = admin_client.post(f"/api/alvs/{alv.id}/parsed_minutes/", json=parsed_data)
    assert rv.status_code == 204, rv.json

    rv = admin_client.get(f"/api/alvs/{alv.id}/parsed_minutes/")
    assert rv.status_code == 200, rv.json
