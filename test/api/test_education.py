from collections.abc import Callable

from app.models.education import Education
from conftest import CustomClient


def test_list_educations(
    anonymous_client: CustomClient, education_factory: Callable[[], Education]
) -> None:
    e = education_factory()
    rv = anonymous_client.get("/api/educations/")
    assert rv.status_code == 200
    assert {
        "id": e.id,
        "nl_name": e.nl_name,
        "en_name": e.en_name,
        "programme_type": e.programme_type.name.lower(),
    } in rv.json["data"]
