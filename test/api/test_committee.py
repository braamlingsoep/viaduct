import pytest
from sqlalchemy.orm import Session

from app.models.committee import CommitteeTag
from app.models.group import Group
from app.models.page import Page, PageRevision
from app.models.user import User
from conftest import CustomClient


@pytest.fixture
def committee_tag(db_session: Session) -> CommitteeTag:
    ct = CommitteeTag()
    ct.nl_name = "tag1"
    ct.en_name = "tag1"
    db_session.add(ct)
    db_session.commit()
    return ct


def test_create_committee(
    admin_client: CustomClient,
    admin_user: User,
    admin_group: Group,
    page_revision: tuple[Page, PageRevision],
    committee_tag: CommitteeTag,
):
    rv = admin_client.post(
        "/api/committees/",
        json={
            "name": {"en": "ICT-Committee", "nl": "ICT-Commissie"},
            "coordinator": {"id": admin_user.id},
            "group": {"id": admin_group.id},
            "page": {"id": page_revision[0].id},
            "coordinator_interim": False,
            "open_new_members": True,
            "tags": [{"id": committee_tag.id}],
            "pressure": 4,
            "description": {"en": "Some description", "nl": "een beschrijving"},
        },
    )
    assert rv.status_code == 200, rv.json


def test_create_committee_description_too_long(
    admin_client: CustomClient,
    admin_user: User,
    admin_group: Group,
    page_revision: tuple[Page, PageRevision],
    committee_tag: CommitteeTag,
):
    rv = admin_client.post(
        "/api/committees/",
        json={
            "name": {"en": "ICT-Committee", "nl": "ICT-Commissie"},
            "coordinator": {"id": admin_user.id},
            "group": {"id": admin_group.id},
            "page": {"id": page_revision[0].id},
            "coordinator_interim": False,
            "open_new_members": True,
            "tags": [{"id": committee_tag.id}],
            "pressure": 4,
            "description": {
                "en": "Some description" * 30,
                "nl": "een beschrijving" * 30,
            },
        },
    )
    assert rv.status_code == 400, rv.json


def test_list_committees(member_client: CustomClient) -> None:
    rv = member_client.get("/api/committees/")
    assert rv.status_code == 200, rv.json


def test_list_committee_tags(admin_client: CustomClient) -> None:
    rv = admin_client.get("/api/committees/tags/")
    assert rv.status_code == 200, rv.json
