import pytest
from sqlalchemy.orm import Session

from app.models.course import EducationCourse
from app.models.user import User
from app.repository import user_repository
from conftest import CustomClient


@pytest.fixture
def course_with_education_link(admin_user, course_factory, education_factory, db):
    e1 = education_factory()
    user_repository.set_user_education(db.session, admin_user, [e1])
    c1 = course_factory()
    ec = EducationCourse(education_id=e1.id, course_id=c1.id, year="1", periods=[1])
    db.session.add(ec)
    db.session.commit()
    return {"education": e1, "course": c1, "education_course": ec}


def test_list_courses(admin_client, course_factory):
    course = course_factory()
    rv = admin_client.get("/api/courses/")
    assert rv.status_code == 200, rv.json
    assert rv.json["data"][0]["id"] == course.id, rv.json


def test_list_courses_by_education(admin_client, course_with_education_link):
    rv = admin_client.get(
        "/api/courses/",
        query_string={
            "user_educations": f"{course_with_education_link['education'].id},1"
        },
    )
    assert rv.status_code == 200, rv.json
    assert rv.json["data"][0]["id"] == course_with_education_link["course"].id, rv.json


def test_list_courses_by_datanose_id(admin_client, course_with_education_link):
    rv = admin_client.get(
        "/api/courses/",
        query_string={"dn_course_ids": f"{course_with_education_link['course'].id}"},
    )
    assert rv.status_code == 200, rv.json
    assert rv.json["data"][0]["id"] == course_with_education_link["course"].id, rv.json


def test_list_user_courses(
    member_client: CustomClient,
    db_session: Session,
    member_user: User,
    course_with_education_link,
    requests_mocker,
):
    requests_mocker.get(
        f"https://api.datanose.nl/Enrolments/{member_user.student_id}",
        json=[
            {
                "CatalogNumber": course_with_education_link["course"].datanose_code,
                "Section": "A",
                "Term": 2211,
                "ClassNumber": 1273,
            },
        ],
    )
    member_user.educations.append(course_with_education_link["education"])
    db_session.commit()

    rv = member_client.get("/api/users/self/courses/")
    assert rv.status_code == 200, rv.json
    assert rv.json[0]["id"] == course_with_education_link["course"].id
