#!/bin/sh
ip=`hostname -i`
cat << EOF

# IMPORT TO READ BEFORE GETTING STARTED!
#
# # Permissions
# Access to any results is determined by matching the users permissions, groups and membership status to the page.
# If the user matches ANY of permission, group_id, or needs_paid they will be granted access. This means that when
# access should not be determined by one or more of these fields, they should never match with the users values.
#
# In case you want to disable access based on one of the three "permissions", you need to set the following default
# values.
# * -1 for group_id
# * -1 for needs_paid
# * Null for permission
#
# Be very careful as Null for an integer will map to 0 and will therefor grant access to everyone if being set to
# needs_paid (or give everyone in group 0 access to the result)
#
#
# # Distributed index id
# All sources select the id with an offset of +N,000,000. This assumes that there will never be more
# 1 million entries from a single source. If id's from sources overlap, manticore's distributed index
# used to search all indices won't be able to function.

searchd {
    listen = 9306:mysql41
    listen = $ip:9312
    listen = 9308:http
    listen = localhost:9308:http

    # more info about replication you can find at
    # https://docs.manticoresearch.com/latest/html/replication.html
    listen = $ip:9315-9325:replication
    log = /var/lib/manticore/log/searchd.log
    # you can also send query_log to /dev/stdout to be shown in docker logs
    query_log = /var/lib/manticore/log/query.log
    read_timeout = 5
    max_children = 30
    pid_file = /var/run/manticore/searchd.pid
    seamless_rotate = 1
    preopen_indexes = 1
    unlink_old = 1
    workers = thread_pool
    binlog_path = /var/lib/manticore/data
    max_packet_size = 128M
    mysql_version_string = 5.5.21
    data_dir = /var/lib/manticore/replication
}

source page {
    type = pgsql
    sql_host = $POSTGRES_HOST
    sql_port = 5432
    sql_user = $POSTGRES_USER
    sql_pass = $POSTGRES_PASS
    sql_db = $POSTGRES_DB
    sql_query =  SELECT DISTINCT ON (p.id) \
                        p.id + 1000000 as id, \
                        -1 as group_id, \
                        CONCAT('/', p.path) as url, \
                        p.needs_paid, \
                        Null as permission, \
                        pr.nl_title, \
                        pr.en_title, \
                        pr.nl_content, \
                        pr.en_content, \
                        CONCAT('Pagina met URL: /', p.path) as nl_sub_title, \
                        CONCAT('Page with URL: /', p.path) as en_sub_title, \
                        Null as img_url, \
                        'page' as index \
                 FROM page_revision pr \
                 LEFT JOIN page p \
                    ON p.id = pr.page_id \
                 WHERE p.deleted is null \
                 ORDER BY p.id, pr.modified desc
    sql_field_string = nl_title
    sql_field_string = en_title
    sql_field_string = nl_content
    sql_field_string = en_content
    sql_field_string = nl_sub_title
    sql_field_string = en_sub_title
    sql_field_string = img_url
    sql_field_string = index
    sql_attr_bigint = id
    sql_attr_uint = group_id
    sql_attr_uint = needs_paid
    sql_attr_string = permission
    sql_field_string = url
}

source examination {
    type = pgsql
    sql_host = $POSTGRES_HOST
    sql_port = 5432
    sql_user = $POSTGRES_USER
    sql_pass = $POSTGRES_PASS
    sql_db = $POSTGRES_DB
    sql_query = SELECT id + 3000000 as id, \
                       -1 as group_id, \
                       1 as needs_paid, \
                       Null as permission, \
                       '/examination' as url,  \
                       name as nl_title, \
                       name as en_title, \
                       '' as nl_content, \
                       '' as en_content, \
                       'Vak in tentamenbank' as nl_sub_title, \
                       'Course in examinations' as en_sub_title, \
                       Null as img_url, \
                       'examination' as index \
                FROM course
    sql_field_string = nl_title
    sql_field_string = en_title
    sql_field_string = nl_content
    sql_field_string = en_content
    sql_field_string = nl_sub_title
    sql_field_string = en_sub_title
    sql_field_string = img_url
    sql_field_string = index
    sql_attr_bigint = id
    sql_attr_uint = group_id
    sql_attr_uint = needs_paid
    sql_attr_string = permission
    sql_field_string = url
}

source activity {
    type = pgsql
    sql_host = $POSTGRES_HOST
    sql_port = 5432
    sql_user = $POSTGRES_USER
    sql_pass = $POSTGRES_PASS
    sql_db = $POSTGRES_DB
    sql_query = SELECT id + 4000000 as id, \
                       -1 as group_id, \
                       CASE WHEN end_time < current_timestamp THEN 1 ELSE Null END as needs_paid, \
                       Null as permission, \
                       CONCAT('/activities/', id) as url,  \
                       nl_name as nl_title, \
                       en_name as en_title, \
                       nl_description as nl_content, \
                       en_description as en_content, \
                       CONCAT(TO_CHAR(start_time :: DATE, 'Dy dd Mon yyyy (HH:MM)'), ' - ', \
                              TO_CHAR(end_time :: DATE, 'Dy dd Mon yyyy (HH:MM)')) as nl_sub_title, \
                       CONCAT(TO_CHAR(start_time :: DATE, 'Dy dd Mon yyyy (HH:MM)'), ' - ', \
                              TO_CHAR(end_time :: DATE, 'Dy dd Mon yyyy (HH:MM)')) as en_sub_title, \
                       CONCAT('/activities/', id, '/picture/thumbnail/') as img_url, \
                       'activity' as index \
                FROM activity
    sql_field_string = nl_title
    sql_field_string = en_title
    sql_field_string = nl_content
    sql_field_string = en_content
    sql_field_string = nl_sub_title
    sql_field_string = en_sub_title
    sql_field_string = img_url
    sql_field_string = index
    sql_attr_bigint = id
    sql_attr_uint = group_id
    sql_attr_uint = needs_paid
    sql_attr_string = permission
    sql_field_string = url
}

source pimpy_minute {
    type = pgsql
    sql_host = $POSTGRES_HOST
    sql_port = 5432
    sql_user = $POSTGRES_USER
    sql_pass = $POSTGRES_PASS
    sql_db = $POSTGRES_DB
    sql_query = SELECT g.id + 5000000 as id, \
                       g.id as group_id, \
                       -1 as needs_paid, \
                       Null as permission, \
                       CONCAT('/pimpy/minutes/single/', pm.id) as url,  \
                       CONCAT('Vergadering van ', g.name, ' op ', TO_CHAR(minute_date :: DATE, 'dd Mon yyyy')) as nl_title, \
                       CONCAT('Meeting of ', g.name, ' on ', TO_CHAR(minute_date :: DATE, 'Mon dd, yyyy')) as en_title, \
                       content as nl_content, \
                       content as en_content, \
                       CONCAT('Notulen in groep: ', g.name) as nl_sub_title, \
                       CONCAT('Minute in group: ', g.name) as en_sub_title, \
                       Null as img_url, \
                       'pimpy_minute' as index \
                FROM pimpy_minute pm \
                LEFT JOIN "group" g \
                  ON g.id = pm.group_id
    sql_field_string = nl_title
    sql_field_string = en_title
    sql_field_string = nl_content
    sql_field_string = en_content
    sql_field_string = nl_sub_title
    sql_field_string = en_sub_title
    sql_field_string = img_url
    sql_field_string = index
    sql_attr_bigint = id
    sql_attr_uint = group_id
    sql_attr_uint = needs_paid
    sql_attr_string = permission
    sql_field_string = url
}

source pimpy_task {
    type = pgsql
    sql_host = $POSTGRES_HOST
    sql_port = 5432
    sql_user = $POSTGRES_USER
    sql_pass = $POSTGRES_PASS
    sql_db = $POSTGRES_DB
    sql_query = SELECT g.id + 6000000 as id, \
                       g.id as group_id, \
                       -1 as needs_paid, \
                       Null as permission, \
                       CONCAT('/pimpy/tasks/', g.id) as url,  \
                       CONCAT(u.first_name, ' ', u.last_name, ': ', pt.title) as nl_title, \
                       CONCAT(u.first_name, ' ', u.last_name, ': ', pt.title) as en_title, \
                       pt.content as nl_content, \
                       pt.content as en_content, \
                       CONCAT('Taak in groep: ', g.name) as nl_sub_title, \
                       CONCAT('Task in group: ', g.name) as en_sub_title, \
                       Null as img_url, \
                       'pimpy_task' as index \
                FROM pimpy_task pt \
                LEFT JOIN "group" g \
                  ON g.id = pt.group_id \
                LEFT JOIN pimpy_task_user ptu \
                  on pt.id = ptu.task_id \
                LEFT JOIN "user" u \
                  on ptu.user_id = u.id
    sql_field_string = nl_title
    sql_field_string = en_title
    sql_field_string = nl_content
    sql_field_string = en_content
    sql_field_string = nl_sub_title
    sql_field_string = en_sub_title
    sql_field_string = img_url
    sql_field_string = index
    sql_attr_bigint = id
    sql_attr_uint = group_id
    sql_attr_uint = needs_paid
    sql_attr_string = permission
    sql_field_string = url
}

source news {
    type = pgsql
    sql_host = $POSTGRES_HOST
    sql_port = 5432
    sql_user = $POSTGRES_USER
    sql_pass = $POSTGRES_PASS
    sql_db = $POSTGRES_DB
    sql_query = SELECT n.id + 2000000 as id, \
                       -1 as group_id, \
                       n.needs_paid as needs_paid, \
                       Null as permission, \
                       CONCAT('/news/', n.id, '/view/') as url,  \
                       n.nl_title as nl_title, \
                       n.en_title as en_title, \
                       n.nl_content as nl_content, \
                       n.en_content as en_content, \
                       CONCAT('Nieuwsbericht door: ', u.first_name, ' ', u.last_name) as nl_sub_title, \
                       CONCAT('News by: ', u.first_name, ' ', u.last_name) as en_sub_title, \
                       Null as img_url, \
                       'news' as index \
                FROM news n \
                LEFT JOIN "user" u \
                  ON u.id = n.user_id \
                WHERE n.publish_date < NOW()
    sql_field_string = nl_title
    sql_field_string = en_title
    sql_field_string = nl_content
    sql_field_string = en_content
    sql_field_string = nl_sub_title
    sql_field_string = en_sub_title
    sql_field_string = img_url
    sql_field_string = index
    sql_attr_bigint = id
    sql_attr_uint = group_id
    sql_attr_uint = needs_paid
    sql_attr_string = permission
    sql_field_string = url
}

# The query for this index takes the list of ALV documents together with all
# parsed events in the ALV and concatenates them to the nl_content column.
source alv {
    type = pgsql
    sql_host = $POSTGRES_HOST
    sql_port = 5432
    sql_user = $POSTGRES_USER
    sql_pass = $POSTGRES_PASS
    sql_db = $POSTGRES_DB
    sql_query = \
        WITH "documents" AS ( \
          SELECT alv_id,  \
          E'Documenten:\n  * ' || STRING_AGG(nl_name, E'\n  * ' ORDER BY nl_name) as "nl_content", \
          E'Documents:\n  * ' || STRING_AGG(en_name, E'\n  * ' ORDER BY en_name) as "en_content" \
          FROM "alv_document" \
          GROUP BY alv_id \
        ), \
        "events" AS ( \
        SELECT alv_id, \
        E'Evenementen:\n  * ' || STRING_AGG(regexp_replace(CASE \
            WHEN event_type = 'TIMESTAMP'::alv_event_type THEN CONCAT(data->>'time', ': ', data->>'event') \
            WHEN event_type = 'SECTION'::alv_event_type THEN data->>'title' \
            WHEN event_type = 'SUBSECTION'::alv_event_type THEN data->>'title' \
            WHEN event_type = 'VOTE'::alv_event_type THEN data->>'title' \
            WHEN event_type = 'DECISION'::alv_event_type THEN data->>'decision' \
            WHEN event_type = 'TASK'::alv_event_type THEN CONCAT(data->>'who', ': ', data->>'what') \
        ELSE '' \
        END, E'[\\n\\r]+', ' ', 'g'), E'\n  * ') as content \
        FROM "alv_event" \
        WHERE event_type != 'PARSE_WARNING'::alv_event_type \
        GROUP BY alv_id \
        ) \
        SELECT  \
          id + 7000000 as id, \
          -1 as group_id, \
          1 as needs_paid, \
          Null as permission, \
          CONCAT('/alv/', id) as url, \
          nl_name as nl_title, \
          en_name as en_title, \
          TO_CHAR(date, 'Dy dd Mon yyyy') as nl_sub_title, \
          TO_CHAR(date, 'Dy dd Mon yyyy') as en_sub_title, \
          Null as img_url, \
          'alv' as index, \
          "documents".en_content as en_content, \
          CONCAT("documents".nl_content, E'\n\n', "events".content) as nl_content \
        FROM "alv" \
        LEFT JOIN "documents" ON "documents".alv_id="alv".id \
        LEFT JOIN "events" ON "events".alv_id="alv".id \
        GROUP BY "alv".id, "documents".nl_content, "documents".en_content, "events".content \
        ORDER BY "alv".date ASC
    sql_field_string = nl_title
    sql_field_string = en_title
    sql_field_string = nl_content
    sql_field_string = en_content
    sql_field_string = nl_sub_title
    sql_field_string = en_sub_title
    sql_field_string = img_url
    sql_field_string = index
    sql_attr_bigint = id
    sql_attr_uint = group_id
    sql_attr_uint = needs_paid
    sql_attr_string = permission
    sql_field_string = url
}

index page
{
    source = page
    path = /var/lib/manticore/data/page
}

index news
{
    source = news
    path = /var/lib/manticore/data/news
}

index examination
{
    source = examination
    path = /var/lib/manticore/data/examination

}

index activity
{
    source = activity
    path = /var/lib/manticore/data/activity

}

index pimpy_minute
{
    source = pimpy_minute
    path = /var/lib/manticore/data/pimpy_minute

}

index pimpy_task
{
    source = pimpy_task
    path = /var/lib/manticore/data/pimpy_task
}

index alv
{
    source = alv
    path = /var/lib/manticore/data/alv
}

index viaduct
{
    type = distributed
    local = news
    local = page
    local = examination
    local = activity
    local = pimpy_minute
    local = pimpy_task
    local = alv
}

EOF
