import locale
from functools import wraps

from flask import (
    Blueprint,
    Response,
    abort,
    render_template,
    request,
)
from flask_babel import lazy_gettext as _

from app import db
from app.decorators import require_role
from app.models.newsletter import Newsletter
from app.roles import Roles
from app.service import page_service
from app.service.news_service import NewsletterCopernicaTokenSettings

blueprint = Blueprint("newsletter", __name__, url_prefix="/newsletter")


@blueprint.route("/", methods=["GET"])
@require_role(Roles.NEWS_WRITE)
def root():
    return render_template("vue_content.htm", title=_("Newsletters"))


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<newsletter:newsletter>/edit/", methods=["GET"])
@require_role(Roles.NEWS_WRITE)
def edit(newsletter=None):
    if newsletter:
        title = _("Edit newsletter")
    else:
        title = _("Create newsletter")
    return render_template("vue_content.htm", title=title)


def correct_token_provided(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        token = request.args.get("auth_token")
        settings = NewsletterCopernicaTokenSettings(db_session=db.session)
        if token and settings.copernica_newsletter_token.get_secret_value() == token:
            return f(*args, **kwargs)
        else:
            return abort(403)

    return wrapper


@blueprint.route("/latest/committees/", methods=["GET"])
@correct_token_provided
def committees_xml():
    pages = page_service.find_all_committee_pages()
    revisions = [page_service.get_latest_revision(page) for page in pages]
    revisions.sort(key=lambda r: r.title)
    new_members = [c for c in revisions if c.open_new_members]

    return Response(
        render_template("newsletter/committees.xml", items=new_members),
        mimetype="text/xml",
    )


@blueprint.route("/<newsletter:newsletter>/activities/<string:lang>/", methods=["GET"])
@blueprint.route("/latest/activities/<string:lang>/", methods=["GET"])
@correct_token_provided
def activities_xml(newsletter: Newsletter | None = None, lang: str = "en"):
    if newsletter is None:
        newsletter = db.session.query(Newsletter).order_by(Newsletter.id.desc()).first()

        if newsletter is None:
            return abort(404)

    # Set locale for the date formatting in the activity template
    if lang == "nl":
        locale.setlocale(locale.LC_ALL, "nl_NL.utf8")
    else:
        locale.setlocale(locale.LC_ALL, "en_US.utf8")

    return Response(
        render_template(
            "newsletter/activities.xml", items=newsletter.activities, lang=lang
        ),
        mimetype="text/xml",
    )


@blueprint.route("/<newsletter:newsletter>/news/<string:lang>/", methods=["GET"])
@blueprint.route("/latest/news/<string:lang>/", methods=["GET"])
@correct_token_provided
def news_xml(newsletter: Newsletter | None = None, lang="en"):
    if newsletter is None:
        newsletter = db.session.query(Newsletter).order_by(Newsletter.id.desc()).first()

        if newsletter is None:
            return abort(404)

    return Response(
        render_template("newsletter/news.xml", items=newsletter.news_items, lang=lang),
        mimetype="text/xml",
    )


@blueprint.route("/<newsletter:newsletter>/greetings/<string:lang>/", methods=["GET"])
@blueprint.route("/latest/greetings/<string:lang>/", methods=["GET"])
@correct_token_provided
def greeting_xml(newsletter: Newsletter | None = None, lang: str = "en") -> Response:
    if newsletter is None:
        newsletter = db.session.query(Newsletter).order_by(Newsletter.id.desc()).first()

        if newsletter is None:
            return abort(404)

    return Response(
        render_template(
            "newsletter/greetings.xml", items=newsletter.greetings, lang=lang
        ),
        mimetype="text/xml",
    )
