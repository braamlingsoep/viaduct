from flask import Blueprint, render_template
from flask_babel import gettext

from app.decorators import require_role
from app.roles import Roles

blueprint = Blueprint("redirect", __name__, url_prefix="/redirect")


@blueprint.route("/", methods=["GET"])
@require_role(Roles.REDIRECT_WRITE)
def view():
    return render_template("vue_content.htm", title=gettext("Redirects"))
