from flask import Blueprint, render_template, request
from flask_babel import _
from flask_login import current_user

from app.decorators import require_role
from app.models.pimpy import Minute
from app.roles import Roles
from app.service import group_service

blueprint = Blueprint("pimpy", __name__, url_prefix="/pimpy")


@blueprint.route("/minutes/", methods=["GET"])
@require_role(Roles.PIMPY_READ)
def view_minutes():
    group_id = request.args.get("group")
    if group_id:
        group_service.check_user_member_of_group(current_user, group_id)
    return render_template("vue_content.htm", title=_("Minutes - Pimpy"))


@blueprint.route("/minutes/single/<minute:minute>/")
@blueprint.route("/minutes/single/<minute:minute>/<int:line_number>/")
@require_role(Roles.PIMPY_READ)
def view_minute(minute: Minute, line_number=-1):
    group_service.check_user_member_of_group(current_user, minute.group_id)
    return render_template("vue_content.htm", title=_("Minute - Pimpy"))


@blueprint.route("/minutes/single/<minute:minute>/raw/")
@require_role(Roles.PIMPY_READ)
def view_minute_raw(minute: Minute):
    group_service.check_user_member_of_group(current_user, minute.group_id)

    return minute.content, {"Content-Type": "text/plain; charset=utf-8"}


@blueprint.route("/", methods=["GET"])
@blueprint.route("/tasks/self/", methods=["GET"], endpoint="view_tasks_self")
@blueprint.route("/tasks/", methods=["GET"])
@require_role(Roles.PIMPY_READ)
def view_tasks():
    group_id = request.args.get("group")
    if group_id is not None:
        group_service.check_user_member_of_group(current_user, group_id)

    return render_template("vue_content.htm", title=_("Tasks - Pimpy"))


@blueprint.route("/tasks/create/", methods=["GET"])
@require_role(Roles.PIMPY_WRITE)
def add_task():
    group_id = request.args.get("group")
    if group_id is not None:
        group_service.check_user_member_of_group(current_user, group_id)
    return render_template("vue_content.htm", title=_("Create task - Pimpy"))


@blueprint.route("/minutes/create/", methods=["GET"])
@require_role(Roles.PIMPY_WRITE)
def add_minute():
    group_id = request.args.get("group")
    if group_id is not None:
        group_service.check_user_member_of_group(current_user, group_id)
    return render_template("vue_content.htm", title=_("New minute - Pimpy"))
