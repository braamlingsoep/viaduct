from datetime import datetime, timedelta

import pytz

from app.models.activity import Activity


class SlideActivityModel:
    """This class contains all data required to display an activity slide."""

    def __init__(self, act: Activity) -> None:
        self.activity = act

        tz = pytz.timezone("Europe/Amsterdam")
        self.start_time = act.start_time.astimezone(tz).replace(microsecond=0)
        self.end_time = act.end_time.astimezone(tz).replace(microsecond=0)

        self.weekdays = [
            (self.start_time + timedelta(days=i)).weekday()
            for i in range((self.end_time - self.start_time).days + 1)
        ]
        self.name = act.name
        self.is_visible = True
        self.cell = -1
        self.has_overlapping_activities = False

    @property
    def current_weekdays(self) -> list[int]:
        return list(range(self.starting_day, self.end_day + 1))

    @property
    def duration(self) -> int:
        return int((self.end_time - self.start_time).total_seconds())

    @property
    def starting_day(self) -> int:
        if self.is_visible and self.start_time < self.get_first_day_current_week():
            return 0
        return self.weekdays[0]

    @property
    def end_day(self) -> int:
        if self.is_visible and self.end_time > self.get_last_day_current_week():
            return 6
        return self.weekdays[-1]

    @property
    def duration_days(self) -> int:
        return self.end_day - self.starting_day + 1

    @property
    def time_start(self) -> str:
        return f"{self.start_time:%H:%M}"

    @property
    def time_end(self) -> str:
        return f"{self.end_time:%H:%M}"

    @property
    def is_past(self) -> bool:
        return self.activity.is_in_past()

    @property
    def is_short(self) -> bool:
        return self.duration <= 60 * 60

    def overlaps(self, other: "SlideActivityModel") -> bool:
        """Return True if this and the other event overlap, False otherwise."""
        return (self.start_time < other.start_time < self.end_time) or (
            other.start_time < self.start_time < other.end_time
        )

    def is_multiday(self) -> bool:
        """Returns True if the activity is 24h or longer, False otherwise."""
        return (self.end_time - self.start_time) >= timedelta(days=1)

    @staticmethod
    def time_to_percentage(time: datetime, start: int = 9, end: int = 24) -> float:
        """
        Return the given time as a percentage (between 0 and 100)
        between 'start' and 'end'.

        For example, time_to_frac(12:00:00, 10, 18) should return 25.
        """
        frac = (time.hour * 60 + time.minute - start * 60) / ((end - start) * 60) * 100
        return max(min(frac, 100), 0)

    @property
    def view_top_spacing(self):
        return self.time_to_percentage(self.start_time)

    @property
    def view_height(self):
        return self.time_to_percentage(self.end_time) - self.view_top_spacing

    @staticmethod
    def get_first_day_current_week() -> datetime:
        today = datetime.today()
        return (today - timedelta(days=today.weekday())).astimezone(
            pytz.timezone("Europe/Amsterdam")
        )

    @staticmethod
    def get_last_day_current_week() -> datetime:
        today = datetime.today()
        return (today + timedelta(days=6 - today.weekday())).astimezone(
            pytz.timezone("Europe/Amsterdam")
        )
