from functools import wraps

from flask import Blueprint, render_template, request
from pydantic import SecretStr
from werkzeug.routing import BaseConverter, ValidationError

from app import app, db, get_locale
from app.exceptions.base import ResourceNotFoundException
from app.service import copernica_service, mailinglist_service
from app.service.setting_service import DatabaseSettingsMixin

blueprint = Blueprint("copernica", __name__)


class CopernicaWebhookVerificiationSettings(DatabaseSettingsMixin):
    copernica_webhook_verification_enabled: bool = False
    copernica_webhook_verification_filename: str = ""
    copernica_webhook_token = SecretStr("")
    copernica_webhook_verification_content = SecretStr("")


class CopernicaWebhookVerificationConverter(BaseConverter):
    """
    Converter to dynamically determine the copernica webhook verification url.

    This converter has a high weight (300) to have a low priority. Pages and
    redirects have higher priority. (200 and 201 respectively).
    """

    weight = 300

    def to_python(self, value):  # pylint: disable=no-self-user
        settings = CopernicaWebhookVerificiationSettings(db_session=db.session)
        enabled = settings.copernica_webhook_verification_enabled
        filename = settings.copernica_webhook_verification_filename
        if not enabled or value != filename:
            raise ValidationError()

        return value

    def to_url(self, value):  # pylint: disable=no-self-user
        settings = CopernicaWebhookVerificiationSettings(db_session=db.session)
        enabled = settings.copernica_webhook_verification_enabled
        filename = settings.copernica_webhook_verification_filename
        if not enabled:
            raise ValidationError()

        return filename


app.url_map.converters[
    "copernica_webhook_verification_filename"
] = CopernicaWebhookVerificationConverter

# We use an auth_token since this is way easier to implement
# than to implement the intended security check correctly:
# https://www.copernica.com/en/documentation/webhook-security


def correct_copernica_webhook_token(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        token = request.args.get("auth_token")
        settings = CopernicaWebhookVerificiationSettings(db_session=db.session)
        if token and token == settings.copernica_webhook_token.get_secret_value():
            return f(*args, **kwargs)
        else:
            # Don't give away whether auth_token was correct
            return ""

    return wrapper


@blueprint.route("/<copernica_webhook_verification_filename:filename>")
def verification(filename):  # pylint: disable=unused-argument
    settings = CopernicaWebhookVerificiationSettings(db_session=db.session)
    return (
        settings.copernica_webhook_verification_content.get_secret_value(),
        {"Content-Type": "text/plain"},
    )


@blueprint.route("/copernica/webhook/", methods=["POST"])
@correct_copernica_webhook_token
def webhook():
    if request.form is not None:
        copernica_service.handle_webhook(request.form)

    return ""


@blueprint.route(
    "/copernica/unsubscribed/<int:mailinglist_id>/<int:profile_id>/<profile_secret>/"
)
def unsubscribed_landingpage(mailinglist_id: int, profile_id: int, profile_secret: str):
    def error():
        return render_template("copernica/unsubscribed.htm", error=True), 400

    try:
        mailinglist = mailinglist_service.get_mailinglist(mailinglist_id)
    except ResourceNotFoundException:
        return error()

    profile = copernica_service.get_profile_info(profile_id)
    if not profile or profile.secret != profile_secret:
        return error()

    mailinglist_name = mailinglist_service.get_mailinglist_name(
        mailinglist, get_locale()
    )

    return render_template(
        "copernica/unsubscribed.htm", error=False, mailinglist_name=mailinglist_name
    )
