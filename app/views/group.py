from flask import Blueprint, render_template
from flask_babel import _

from app.decorators import require_role
from app.models.group import Group
from app.roles import Roles

blueprint = Blueprint("group", __name__, url_prefix="/groups")


@blueprint.route("/", methods=["GET"])
@require_role(Roles.GROUP_READ)
def view():
    return render_template(
        "vue_content.htm", title=_("Groups"), keep_alive_include="group-overview"
    )


@blueprint.route("/create/", methods=["GET"])
@require_role(Roles.GROUP_WRITE)
def create():
    return render_template("vue_content.htm", title=_("Create group"))


@blueprint.route("/<group:__>/edit/", methods=["GET"])
@require_role(Roles.GROUP_WRITE)
def edit(__: int):
    return render_template("vue_content.htm", title=_("Edit group"))


@blueprint.route("/<group:group>/users/", methods=["GET"])
@require_role(Roles.GROUP_READ)
def view_users(group: Group):
    return render_template("vue_content.htm", title="%s users" % group.name)


@blueprint.route("/<group:group>/roles/", methods=["GET", "POST"])
@require_role(Roles.GROUP_PERMISSIONS)
def roles(group: Group):
    return render_template("vue_content.htm", title="%s roles" % group.name)
