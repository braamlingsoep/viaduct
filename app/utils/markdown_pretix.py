import os
import re
import xml.etree.ElementTree as etree

from flask import url_for
from flask_babel import gettext as _
from flask_login import current_user
from markdown.blockprocessors import BlockProcessor
from markdown.extensions import Extension

from app import app, get_locale
from app.exceptions.base import ServiceUnavailableException


class MarkdownPretixWidgetExtension(Extension):
    def extendMarkdown(self, md):
        md.registerExtension(self)
        md.parser.blockprocessors.register(
            PretixWidgetBlockProcessor(md.parser), "pretix-widget", 160
        )


class PretixWidgetBlockProcessor(BlockProcessor):
    # [pretix "event-slug"]
    RE = re.compile(r"\[\s*pretix\s*(\"|')([a-zA-Z0-9.-]+)\1\s*\]")

    def test(self, parent, block):
        return bool(self.RE.search(block))

    @property
    def user(self):
        return current_user

    def render_pretix_widget(self, parent, match, block, slug):
        # FIXME circular import
        from app.service import pretix_service

        e = etree.SubElement(parent, "div")
        # Parse all text in front of [pretix "slug"]
        self.parser.parseBlocks(e, [block[: match.start(0)]])

        event_url = pretix_service.get_event_url(slug)

        script = etree.Element("script")
        script.set("type", "text/javascript")
        script.set("src", f"https://pretix.svia.nl/widget/v1.{get_locale()}.js")
        script.set("async", "")

        link = etree.Element("link")
        link.set("rel", "stylesheet")
        link.set("type", "text/css")
        link.set("href", os.path.join(event_url, "widget/v1.css"))

        pretix_widget = etree.Element("pretix-widget")
        pretix_widget.set("event", event_url)

        fernet_token = pretix_service.get_fernet_token_from_user(self.user)
        url = url_for("api.pretix.user", fernet_token=fernet_token, _external=True)
        pretix_widget.set("data-user-info-url", url)
        if app.debug:
            pretix_widget.set("skip-ssl-check", "")

        e.append(script)
        e.append(link)
        e.append(pretix_widget)

        # Parse all text after [pretix "slug"]
        self.parser.parseBlocks(e, [block[match.end(0) :]])

    def run(self, parent, blocks):
        # FIXME circular import
        from app.service import pretix_service

        raw_block = blocks.pop(0)

        match = self.RE.search(raw_block)
        if not match:
            return False

        try:
            event = pretix_service.get_event(slug=match.group(2))
            user_state = pretix_service.user_widget_state(event, self.user)
            if user_state == "login":
                return self.render_login_warning(parent)
            if user_state == "testmode":
                return self.render_testmode_warning(parent)
            elif user_state == "favourer_disallowed":
                return self.render_favourer_warning(parent)
            elif user_state == "membership":
                return self.render_membership_warning(parent)
            elif user_state == "register":
                return self.render_pretix_widget(parent, match, raw_block, event.slug)
            return False
        except ServiceUnavailableException as e:
            return self.render_pretix_error(parent, str(e))

    # This could be merged witht the logic in
    def render_favourer_warning(self, parent):
        e = etree.SubElement(parent, "div")
        e.set("class", "alert alert-info")
        e.text = _(
            "You are not able to view the shop, as favourers (Vriend van VIA) "
            "are not permitted to view this shop."
        )

    def render_testmode_warning(self, parent):
        e = etree.SubElement(parent, "div")
        e.set("class", "alert alert-warning")
        e.text = _(
            "You are not able view the shop, as it is still in test mode. "
            "Please come back at a later moment."
        )

    def render_login_warning(self, parent):
        e = etree.SubElement(parent, "div")
        e.set("class", "alert alert-info")
        e.text = _("You must be logged-in to view this shop. ")
        link = etree.SubElement(e, "a")
        link.set("href", url_for("login.sign_in"))
        link.text = _("Log in using this page.")

    def render_membership_warning(self, parent):
        e = etree.SubElement(parent, "div")
        e.set("class", "alert alert-info")
        e.text = _(
            "You need to have valid membership to view this shop. "
            "For more information, visit the "
        )
        link = etree.SubElement(e, "a")
        link.set("href", "/lidmaatschap")
        link.text = _("membership page")

    def render_pretix_error(self, parent, message):
        e = etree.SubElement(parent, "div")
        e.set("class", "alert alert-danger")
        e.text = message
