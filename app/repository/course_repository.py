from flask_sqlalchemy import Pagination
from sqlalchemy import and_, select, tuple_, update

from app import db
from app.api.schema import PageCourseSearchParameters, PageSearchParameters
from app.models.course import Course, EducationCourse
from app.repository.utils.pagination import search_columns


def get_courses_by_ids(course_ids: list[int]) -> list[Course]:
    return (
        db.session.query(Course)
        .filter(Course.id.in_(course_ids))
        .order_by(Course.name)
        .all()
    )


def get_courses_by_datanose_codes(datanose_codes: list[str]) -> list[Course]:
    return (
        db.session.query(Course)
        .filter(Course.datanose_code.in_(datanose_codes))
        .order_by(Course.name)
        .all()
    )


# TODO https://gitlab.com/studieverenigingvia/viaduct/-/issues/1021 add testcase
def get_education_course_by_exclusion_ids(
    ignored_pairs: list[tuple[int, int]], education_ids: list[int]
):
    """
    Returns all education_course entries not in the list of pairs, and with
    a provided education id.
    """
    return (
        db.session.query(EducationCourse.education_id, EducationCourse.course_id)
        .filter(
            tuple_(EducationCourse.education_id, EducationCourse.course_id).notin_(
                ignored_pairs
            )
        )
        .filter(EducationCourse.education_id.in_(education_ids))
        .all()
    )


def paginated_get_courses_by_ids(
    course_ids: list[int], pagination: PageCourseSearchParameters
) -> Pagination:
    return (
        db.session.query(Course)
        .filter(Course.id.in_(course_ids))
        .order_by(Course.name)
        .paginate(pagination.page, pagination.limit, True)
    )


def get_course_ids_by_education_year(
    education_year: list[tuple[int, int]]
) -> list[int]:
    """
    Get list of courses that are of a given education-year combination.
    :type education_year: (education_id, year).
    """
    stmt = select(EducationCourse.course_id).filter(
        tuple_(EducationCourse.education_id, EducationCourse.year).in_(education_year)
    )
    return db.session.execute(stmt).scalars().all()


def paginated_search_all_courses(pagination: PageSearchParameters) -> Pagination:
    q = db.session.query(Course).order_by(Course.name.asc())
    q = search_columns(q, pagination.search, Course.name, Course.datanose_code)
    return q.paginate(pagination.page, pagination.limit, error_out=False)


def find_course(name=None, datanose_code=None) -> Course | None:
    """Get a course that has the exact name or datanose_code as is provided."""
    if datanose_code:
        return db.session.query(Course).filter_by(datanose_code=datanose_code).first()
    elif name:
        return db.session.query(Course).filter_by(name=name).first()
    else:
        return None


def get_education_course(education_id: int, course_id: int) -> int:
    return (
        db.session.query(EducationCourse.course_id)
        .filter(
            EducationCourse.education_id == education_id,
            EducationCourse.course_id == course_id,
        )
        .scalar()
    )


def add_education_course(
    education_id: int, course_id: int, year: int = None, periods: list[int] = None
):
    edu_course_link = EducationCourse(
        education_id=education_id, course_id=course_id, year=year, periods=periods
    )
    db.session.add(edu_course_link)
    db.session.commit()


def update_education_course_year_periods(
    education_id: int, course_id: int, year: int | None, periods: list[int] | None
):
    stmt = (
        update(EducationCourse)
        .where(
            and_(
                EducationCourse.education_id == education_id,
                EducationCourse.course_id == course_id,
            )
        )
        .values(year=year, periods=periods)
        .execution_options(synchronize_session="fetch")
    )
    db.session.execute(stmt)
    db.session.commit()


def create_course() -> Course:
    return Course()


def save_course(course: Course) -> Course:
    db.session.add(course)
    db.session.commit()
    return course


def delete_course(course: Course):
    db.session.delete(course)
    db.session.commit()
