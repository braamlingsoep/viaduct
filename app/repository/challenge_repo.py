import datetime

from sqlalchemy import and_, delete
from sqlalchemy.orm import Session

from app import db
from app.models.challenge import Challenge, Competitor, Submission


def find_with_approved_submission(
    closed, user_id
) -> list[tuple[Challenge, Submission | None]]:
    q = db.session.query(Challenge, Submission).outerjoin(
        Submission,
        and_(
            Challenge.id == Submission.challenge_id,
            Submission.user_id == user_id,
            Submission.approved.is_(True),
        ),
    )
    if not closed:
        q = q.filter(
            Challenge.start_date <= datetime.date.today(),
            Challenge.end_date >= datetime.date.today(),
        )

    return q.all()


def clear_all_competitors(db_session: Session) -> None:
    stmt = delete(Competitor)
    db_session.execute(stmt)


def clear_all_submissions(db_session: Session) -> None:
    stmt = delete(Submission)
    db_session.execute(stmt)
