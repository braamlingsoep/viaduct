import logging
from collections import defaultdict

from sqlalchemy import select

from app import db
from app.models.group import Group, UserGroup
from app.models.role_model import GroupRole
from app.roles import Roles

_logger = logging.getLogger(__name__)


def load_user_roles(user_id: int) -> list[Roles]:
    stmt = (
        select(GroupRole.role)
        .join(Group)
        .join(UserGroup)
        .filter(
            UserGroup.user_id == user_id,
            Group.deleted_at.is_(None),
        )
        .group_by(GroupRole.role)
    )
    rv: list[Roles] = []
    for role in db.session.scalars(stmt):
        try:
            rv.append(Roles[role])
        except KeyError:
            # Handle unknown roles gracefully.
            pass
    return rv


def load_user_roles_with_groups(user_id: int) -> dict[Roles, list[Group]]:
    stmt = (
        select(GroupRole.role, Group)
        .join(Group)
        .join(UserGroup)
        .filter(
            UserGroup.user_id == user_id,
            Group.deleted_at.is_(None),
        )
    )
    roles_with_groups = defaultdict(list)
    for role, group in db.session.execute(stmt):
        try:
            roles_with_groups[Roles[role]].append(group)
        except KeyError:
            # Handle unknown roles gracefully.
            pass
    return roles_with_groups


def load_user_groups_with_roles(user_id: int) -> dict[Group, list[Roles]]:
    stmt = (
        select(GroupRole.role, Group)
        .join(Group)
        .join(UserGroup)
        .filter(
            UserGroup.user_id == user_id,
            Group.deleted_at.is_(None),
        )
    )
    groups_with_roles = defaultdict(list)
    for role, group in db.session.execute(stmt):
        try:
            groups_with_roles[group].append(Roles[role])
        except KeyError:
            # Handle unknown roles gracefully.
            pass
    return groups_with_roles


def find_all_roles_by_group_id(group_id: int) -> set[Roles]:
    stmt = (
        select(GroupRole)
        .join(Group)
        .filter(
            Group.id == group_id,
            Group.deleted_at.is_(None),
        )
        .order_by(GroupRole.role)
    )
    rv = []
    for role in db.session.scalars(stmt):
        try:
            rv.append(Roles[role.role])
        except KeyError:
            # Handle unknown roles gracefully.
            pass
    return set(rv)


def delete_roles_by_group(group_id, removed_roles):
    roles = [role.name for role in removed_roles]
    db.session.query(GroupRole).filter(
        GroupRole.group_id == group_id, GroupRole.role.in_(roles)
    ).delete(synchronize_session="fetch")
    db.session.commit()


def insert_roles_by_group(group_id, added_roles):
    roles = []
    for role in added_roles:
        group_role = GroupRole()
        group_role.group_id = group_id
        group_role.role = role.name
        roles.append(group_role)

    db.session.add_all(roles)
    db.session.commit()


def get_groups_with_role(role: Roles) -> list[Group]:
    return db.session.scalars(
        select(Group)
        .join(GroupRole)
        .filter(
            GroupRole.role == role.name,
            Group.deleted_at.is_(None),
        )
    ).all()
