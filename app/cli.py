import os
import re
from datetime import date
from pathlib import PurePath

import alembic
import alembic.config
import bcrypt
import click
import sqlalchemy
from flask import current_app
from flask.cli import AppGroup
from fuzzywuzzy import fuzz
from sqlalchemy import inspect, text
from sqlalchemy.dialects.postgresql import insert
from unidecode import unidecode
from werkzeug.datastructures import FileStorage

from app import app
from app.enums import ProgrammeType
from app.exceptions.base import ResourceNotFoundException
from app.extensions import db
from app.models.course import Course, EducationCourse
from app.models.education import Education
from app.models.examination import ExamFileType
from app.models.group import Group
from app.models.navigation import NavigationEntry
from app.models.oauth.client import OAuthClient
from app.models.role_model import GroupRole
from app.models.setting_model import Setting
from app.models.user import User
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import (
    course_service,
    education_service,
    examination_service,
    group_service,
)
from app.service.navigation_service import create_entry
from app.utils.flask_init import (
    require_login_manager,
)

administrators = AppGroup("admin", help="Add or remove users from administrators group")
app.cli.add_command(administrators)


@app.cli.command("translations")
def translations():
    i18n = "app/translations"
    config = "babel.cfg"
    messages = "messages.pot"
    os.system(
        f"pybabel extract -F {config} "
        + f"--sort-output --no-location -k lazy_gettext -o {messages} ."
    )
    os.system(f"pybabel update -i {messages} -d {i18n}")
    os.system(f"rm {messages}")
    os.system(f"pybabel compile -d {i18n}")


def _add_group(name):
    existing_group = db.session.query(Group).filter(Group.name == name).first()
    if existing_group:
        print(f"-> Group '{name}' already exists.")
    else:
        try:
            db.session.add(Group(name, None))
            db.session.commit()
            print(f"-> Group '{name}' added.")
        except sqlalchemy.exc.IntegrityError:
            db.session.rollback()


def _add_user(user, error_msg="User already exists"):
    existing_user = db.session.query(User).filter(User.email == user.email).first()
    if existing_user:
        print(f"-> {error_msg}.")
    try:
        db.session.add(user)
        db.session.commit()
        print(f"-> Group '{user.email}' added.")
    except sqlalchemy.exc.IntegrityError:
        db.session.rollback()
        raise


def _add_navigation(entries, parent=None):
    # Parent id of the root is None.
    if not parent:
        parent_id = None
    else:
        parent_id = parent.id

    for pos, (type, nl_title, en_title, url, children) in enumerate(entries):
        with current_app.test_request_context():
            navigation_entry = (
                db.session.query(NavigationEntry)
                .filter(NavigationEntry.en_title == en_title)
                .first()
            )

        if not navigation_entry:
            print(f"-> Added {en_title}")
            with current_app.test_request_context():
                navigation_entry = create_entry(
                    type, nl_title, en_title, parent_id, pos + 1, url, False, False
                )
        else:
            print(f"-> Navigation entry {en_title} exists")

        _add_navigation(children, navigation_entry)


@app.cli.command("dropdb")
def dropdb():
    db.drop_all()


@app.cli.command("createdb")
@require_login_manager
@click.option("--create-admin/--no-create-admin", default=True)
@click.option("--skip-exists/--no-skip-exists", default=False)
def createdb(create_admin: bool, skip_exists: bool):
    """Create a new empty database with (optionally) a single administrator."""
    print("* Creating database schema")

    if skip_exists:
        tables = inspect(db.engine).get_table_names()
        if "alembic_version" in tables:
            print("+ Not creating schema: alembic_version already exist")
            return

    # Enable citext extension
    db.session.execute(text("create extension if not exists citext"))
    db.session.commit()

    # Create the database schema
    db.create_all()

    print("* Adding alembic stamp")

    # Create alembic_version table
    migrations_directory = current_app.extensions["migrate"].directory
    config = alembic.config.Config(os.path.join(migrations_directory, "alembic.ini"))
    config.set_main_option("script_location", migrations_directory)
    alembic.command.stamp(config, "head", purge=True)

    # Add required groups
    print("* Adding administrators' and 'BC' groups")
    _add_group("administrators")
    _add_group("BC")

    # Add educations, which must be present to create the administrator user
    print("* Adding educations")
    education_names = [
        ("Informatica", "Computer Science", ProgrammeType.BACHELOR, "BSc IN"),
        (
            "Kunstmatige Intelligentie",
            "Artificial Intelligence",
            ProgrammeType.BACHELOR,
            "BSc KI",
        ),
        ("Informatiekunde", "Information Science", ProgrammeType.BACHELOR, "BSc IK"),
        ("Information Studies", "Information Studies", ProgrammeType.MASTER, "MSc IS"),
        (
            "Software Engineering",
            "Software Engineering",
            ProgrammeType.MASTER,
            "MSc SE",
        ),
        (
            "Security and Network Engineering",
            "Security and Network Engineering",
            ProgrammeType.MASTER,
            "MSc SNE",
        ),
        (
            "Artificial Intelligence",
            "Artificial Intelligence",
            ProgrammeType.MASTER,
            "MSc AI",
        ),
        (
            "Computational Science",
            "Computational Science",
            ProgrammeType.MASTER,
            "MSc CLSJD",
        ),
        ("Computer Science", "Computer Science", ProgrammeType.MASTER, "MSc CPS"),
        ("Medial Informatics", "Medial Informatics", ProgrammeType.MASTER, None),
        ("Grid Computing", "Grid Computing", ProgrammeType.MASTER, None),
        (
            "Bioinformatics and Systems Biology",
            "Bioinformatics and Systems Biology",
            ProgrammeType.MASTER,
            "MSc BSBJD",
        ),
        ("Logic", "Logic", ProgrammeType.MASTER, "MSc Logic"),
        ("Programmeren", "Programming", ProgrammeType.MINOR, "Min Prog"),
        ("Informatica", "Computer Science", ProgrammeType.MINOR, "Min IN"),
        (
            "Kunstmatige Intelligentie",
            "Artificial Intelligence",
            ProgrammeType.MINOR,
            "Min KI",
        ),
        ("Anders", "Other", ProgrammeType.OTHER, None),
    ]

    for nl_name, en_name, p_type, datanose_code in education_names:
        with current_app.test_request_context():
            if (
                not db.session.query(Education)
                .filter(
                    Education.nl_name == nl_name,
                    Education.en_name == en_name,
                    Education.programme_type == p_type,
                )
                .first()
            ):
                if datanose_code:
                    education = Education(
                        nl_name=nl_name,
                        en_name=en_name,
                        programme_type=p_type,
                        datanose_code=datanose_code,
                        is_via_programme=True,
                    )
                else:
                    education = Education(
                        nl_name=nl_name, en_name=en_name, programme_type=p_type
                    )

                db.session.add(education)
            else:
                print(f"-> Education {en_name} exists")

    db.session.commit()

    # Add courses for the Master Artificial Intelligence.
    print("* Adding courses")
    course_names = [
        ("Machine Learning 1", "52041MAL6Y"),
        ("Deep Learning for Natural Language Processing", "5204DLNL6Y"),
        ("Information Theory", "5314INTH6Y"),
    ]

    for name, datanose_code in course_names:
        with current_app.test_request_context():
            try:
                # Try to find a course with this datanose id.
                course_service.get_course(datanose_code=datanose_code)
                print(f"-> Course {name} exists")
            except ResourceNotFoundException:
                # If course not found, create new course.
                course = Course(name=name, datanose_code=datanose_code)
                db.session.add(course)

    db.session.commit()

    # Add education_course links for the previously generated courses.
    print("* Adding education_courses")
    master_ai = education_service.get_education_by_datanose_code("MSc AI")
    courses = course_service.get_courses_by_datanose_codes([c[1] for c in course_names])
    education_course_links = [(master_ai.id, c.id) for c in courses]

    for education_id, course_id in education_course_links:
        with current_app.test_request_context():
            if not course_service.get_education_course(education_id, course_id):
                link = EducationCourse(education_id=education_id, course_id=course_id)
                db.session.add(link)
            else:
                print(f"-> EducationCourse ({education_id}, {course_id}) exists")

    db.session.commit()

    # Add examinations.
    print("* Adding Examinations")
    if examination_service.count_examinations_by_course(courses[0]) == 0:
        exam_file_path = PurePath(__file__).parent.parent.joinpath(
            "test/fixtures/examination"
        )

        # Add exam file in database.
        with open(
            exam_file_path.joinpath("examination_exam_mock.pdf"), "rb"
        ) as exam_fp:
            # Add exam without answers or summary.
            exam_file = FileStorage(exam_fp)
            exam = examination_service.add_examination(
                date(2020, 10, 22), "Without answers", courses[0].id, "End-term"
            )
            examination_service.save_exam_file(
                exam, exam_file, ExamFileType.EXAMINATION
            )
            db.session.add(exam)

            # Add exam with answers file.
            with open(
                exam_file_path.joinpath("examination_answer_mock.pdf"), "rb"
            ) as answer_fp:
                answer_file = FileStorage(answer_fp)
                exam = examination_service.add_examination(
                    date(2021, 1, 21), "With answers", courses[0].id, "Retake"
                )
                examination_service.save_exam_file(
                    exam, exam_file, ExamFileType.EXAMINATION
                )
                examination_service.save_exam_file(
                    exam, answer_file, ExamFileType.ANSWERS
                )
                db.session.add(exam)

            # Add exam with summary file.
            with open(
                exam_file_path.joinpath("examination_summary_mock.pdf"), "rb"
            ) as summary_fp:
                summary_file = FileStorage(summary_fp)
                exam = examination_service.add_examination(
                    date(2021, 1, 21), "With summary", courses[0].id, "Mid-term"
                )
                examination_service.save_exam_file(
                    exam, exam_file, ExamFileType.EXAMINATION
                )
                examination_service.save_exam_file(
                    exam, summary_file, ExamFileType.SUMMARY
                )
                db.session.add(exam)
    else:
        print(f"-> Course {courses[0].name} already has exams")

    db.session.commit()

    # Add some default navigation
    print("* Adding default navigation entries")
    navigation_entries = [
        (
            "url",
            "Vereniging",
            "Association",
            "/via",
            [
                ("url", "Nieuws", "News", "/news/", []),
                ("url", "PimPy", "PimPy", "/pimpy", []),
                ("url", "Commissies", "Committees", "/commissie", []),
                (
                    "url",
                    "Admin",
                    "Admin",
                    "/admin",
                    [
                        ("url", "Navigatie", "Navigation", "/navigation", []),
                        ("url", "Formulieren", "Forms", "/forms", []),
                        ("url", "Redirect", "Redirect", "/redirect", []),
                        ("url", "Users", "Users", "/users", []),
                        ("url", "Groups", "Groups", "/groups", []),
                        ("url", "Files", "Files", "/files", []),
                    ],
                ),
            ],
        ),
        (
            "activities",
            "Activiteiten",
            "Activities",
            "/activities",
            [
                (
                    "url",
                    "Activiteiten Archief",
                    "Activities archive",
                    "/activities/archive",
                    [],
                ),
                (
                    "url",
                    "Activiteiten Overzicht",
                    "Activities overview",
                    "/activities/view",
                    [],
                ),
            ],
        ),
        ("url", "Vacatures", "Vacancies", "/vacancies/", []),
        ("url", "Tentamenbank", "Examinations", "/examination", []),
    ]

    _add_navigation(navigation_entries)

    print("* Adding administrator user")

    if create_admin:
        email_regex = re.compile(r"^[^@]+@[^@]+\.[^@]+$")
        while True:
            email = click.prompt("\tEmail")
            if email_regex.match(email):
                break
            print("\tInvalid email address: " + email)

        # Add admin user to administrators group
        admin_group = (
            db.session.query(Group).filter_by(name="administrators").one_or_none()
        )

        if db.session.query(User).filter_by(email=email).first() is not None:
            print(f"-> User '{email}' exists")
        else:
            passwd_plain = click.prompt(
                "\tPassword", hide_input=True, confirmation_prompt=True
            )
            first_name = click.prompt("\tFirst name")
            last_name = click.prompt("\tLast name")

            passwd = bcrypt.hashpw(
                passwd_plain.encode("utf-8"), bcrypt.gensalt()
            ).decode("utf-8")
            admin = User(
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=passwd,
                educations=[master_ai],
                phone_nr="",
                tfa_enabled=False,
                student_id="11302968",
                student_id_confirmed=True,
                address="",
                zip="",
                city="",
                locale="en",
            )
            admin.has_paid = True  # type: ignore[assignment]
            _add_user(admin, f"A user with email '{email}' already exists")

            group_service.add_group_users(admin_group, [admin.id])
            db.session.commit()

        # Upsert the developer mail for local testing.
        print(f"* Adding {email} as DEVELOPER_MAIL setting.")
        db.session.execute(
            insert(Setting.__table__)
            .values(key="DEVELOPER_MAIL", value=email)
            .on_conflict_do_update(index_elements=["key"], set_=dict(value=email))
        )
        db.session.commit()

        roles = []
        for role in Roles:
            group_role = GroupRole()
            group_role.group_id = admin_group.id
            group_role.role = role.name
            roles.append(group_role)

        # Grant read/write privilege to administrators group on every module
        db.session.bulk_save_objects(roles)
        db.session.commit()

    print("* Adding public OAuth client")

    client_id = "public"
    client_secret = "public"
    oauth_client = (
        db.session.query(OAuthClient).filter_by(client_id=client_id).one_or_none()
    )
    if oauth_client is None:
        oauth_client = (
            db.session.query(OAuthClient)
            .filter_by(client_secret=client_secret)
            .one_or_none()
        )
    if oauth_client is not None:
        print(f"-> OAuth Client {client_id}:{client_secret} exists")
    else:
        oauth_client = OAuthClient(  # type: ignore
            client_id=client_id,
            client_secret=client_secret,
            auto_approve=False,
        )
        oauth_client.set_client_metadata(
            {
                "client_name": "Development Client",
                "response_types": ["code", "token"],
                "scope": "\n".join(s.name for s in Scopes),
                "redirect_uris": ["https://localhost:5000"],
                "grant_types": [
                    "authorization_code",
                    "implicit",
                    "refresh_token",
                    "password",
                ],
                "token_endpoint_auth_method": "client_secret_post",
            }
        )
        db.session.add(oauth_client)
        db.session.commit()

    print("Done!")


def _administrators_action(user_search, remove):
    """Add or remove users in the administrators group."""
    admin_group = db.session.query(Group).filter(Group.name == "administrators").first()
    if admin_group is None:
        print("Administrators group does not exist.")
        return

    if user_search.isdigit():
        # Search for user ID
        user_id = int(user_search)
        user_found = db.session.query(User).get(user_id)
        if user_found is None or user_id == 0:
            print(f"User with ID {user_id} does not exist.")
            return
    else:
        # Search in user name
        users = db.session.query(User).all()

        maximum = 0
        user_found = None

        # Find user with highest match ratio
        for user in users:
            if user.id == 0:
                continue

            first_name = unidecode(user.first_name.lower().strip())
            last_name = unidecode(user.last_name.lower().strip())

            rate_first = fuzz.ratio(first_name, user_search)
            rate_last = fuzz.ratio(last_name, user_search)

            full_name = first_name + " " + last_name
            rate_full = fuzz.ratio(full_name, user_search)

            if rate_first > maximum or rate_last > maximum or rate_full > maximum:
                maximum = max(rate_first, max(rate_last, rate_full))
                user_found = user

        if user_found is None:
            print("No user found")
            return

    print(f"Found user: {user_found.name} (ID {user_found.id})")
    if admin_group in group_service.get_groups_for_user(user_found):
        if not remove:
            print("User is already in administrators group")
            return
    elif remove:
        print("User is not in administrators group")
        return

    if remove:
        prompt = f"Remove {user_found.name} from administrators group?"
    else:
        prompt = f"Add {user_found.name} to administrators group?"

    if click.confirm(prompt):
        if remove:
            group_service.remove_group_users(admin_group, user_found.id)
        else:
            group_service.add_group_users(admin_group, user_found.id)

        db.session.commit()
        print("User successfully {}.".format("removed" if remove else "added"))


@administrators.command(help="User ID or name")
@click.argument("user")
def add(user):
    """Add a user to the administrator group."""
    _administrators_action(user, False)


@administrators.command(help="User ID or name")
@click.argument("user")
def remove(user):
    """Remove a user from the administrator group."""
    _administrators_action(user, True)
