import logging
from datetime import date

from werkzeug.datastructures import FileStorage

from app.api.schema import PageJobSearchParameters
from app.enums import FileCategory
from app.exceptions.base import (
    BusinessRuleException,
    DuplicateResourceException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.company import Company
from app.models.company_banner import CompanyBanner
from app.models.company_job import CompanyJob
from app.models.company_profile import CompanyProfile
from app.repository import company_repository
from app.service import file_service

_logger = logging.getLogger(__name__)


def find_company_by_id(company_id: int) -> Company | None:
    return company_repository.find_company_by_id(company_id)


def find_company_by_slug(company_slug: str) -> Company | None:
    return company_repository.find_company_by_slug(company_slug)


def find_job_by_id(job_id: int) -> CompanyJob | None:
    return company_repository.find_job_by_id(job_id)


def get_company_by_id(company_id: int) -> Company:
    company = find_company_by_id(company_id)
    if not company:
        raise ResourceNotFoundException("company", company_id)
    return company


def get_company_by_slug(company_slug: str) -> Company:
    company = find_company_by_slug(company_slug)
    if not company:
        raise ResourceNotFoundException("company", company_slug)
    return company


def find_all_companies(filter_inactive=True) -> list[Company]:
    return company_repository.find_all_companies(fitler_inactive=filter_inactive)


def get_job_by_id(job_id: int) -> CompanyJob:
    company_job = find_job_by_id(job_id)
    if company_job is None:
        raise ResourceNotFoundException("company job", job_id)

    return company_job


def create_company(
    name: str,
    slug: str,
    website: str,
    contract_start_date: date,
    contract_end_date: date,
) -> Company:
    if company_repository.company_name_exists(name=name):
        raise DuplicateResourceException("company", "name")

    if company_repository.company_slug_exists(slug=slug):
        raise DuplicateResourceException("Company", "slug")

    if contract_start_date >= contract_end_date:
        raise ValidationException("Contract start date must be before end date")

    # TODO check if slug is lower-case alphanumerical.
    company = company_repository.create_company(
        name=name,
        slug=slug,
        website=website,
        contract_start_date=contract_start_date,
        contract_end_date=contract_end_date,
    )
    return company


def edit_company(
    company_id: int,
    name: str,
    slug: str,
    website: str,
    contract_start_date: date,
    contract_end_date: date,
) -> Company:
    company = get_company_by_id(company_id)

    if company_repository.company_name_exists(name=name, current_company=company):
        raise BusinessRuleException("Company with this name exists.")

    if company_repository.company_slug_exists(slug=slug, current_company=company):
        raise BusinessRuleException("Company with this slug exists.")

    if contract_start_date >= contract_end_date:
        raise ValidationException("Contract start date must be before end date")

    company_repository.edit_company(
        company_id=company_id,
        name=name,
        slug=slug,
        website=website,
        contract_start_date=contract_start_date,
        contract_end_date=contract_end_date,
    )
    return get_company_by_id(company_id)


# TODO Fix the parameter lijst vanuit marshmallow
def create_company_job(
    company_id: int,
    enabled: bool,
    start_date: date,
    end_date: date,
    title_nl: str,
    title_en: str,
    description_nl: str,
    description_en: str,
    contact_name: str,
    contact_email: str,
    contact_address: str,
    contact_city: str,
    website: str,
    contract_of_service: str,
    phone=None,
):
    company = get_company_by_id(company_id)

    if start_date >= end_date:
        raise ValidationException("Job start date must be before end date")

    return company_repository.create_job(
        company=company,
        enabled=enabled,
        start_date=start_date,
        end_date=end_date,
        title_nl=title_nl,
        title_en=title_en,
        description_nl=description_nl,
        description_en=description_en,
        contact_name=contact_name,
        contact_email=contact_email,
        contact_address=contact_address,
        contact_city=contact_city,
        website=website,
        phone=phone,
        contract_of_service=contract_of_service,
    )


def create_company_banner(
    company_id: int,
    start_date: date,
    end_date: date,
    # TODO make website not nullable.
    enabled: bool,
    website: str | None,
) -> CompanyBanner:
    if start_date >= end_date:
        raise ValidationException("Job start date must be before end date")

    company = get_company_by_id(company_id)
    existing_banner = company_repository.find_banner_by_company_id(company.id)
    if existing_banner is not None:
        raise DuplicateResourceException("company banner of company", company_id)

    return company_repository.create_company_banner(
        company_id=company_id,
        start_date=start_date,
        end_date=end_date,
        website=website,
        enabled=enabled,
    )


def create_company_profile(
    company_id: int,
    start_date: date,
    end_date: date,
    enabled: bool,
    description_nl: str,
    description_en: str,
) -> CompanyProfile:
    if start_date >= end_date:
        raise ValidationException("Profile start date must be before end date")

    company = get_company_by_id(company_id)
    existing_profile = company_repository.find_profile_by_company_id(company.id)
    if existing_profile is not None:
        raise DuplicateResourceException("company profile of company", company_id)
    return company_repository.create_company_profile(
        company_id=company_id,
        start_date=start_date,
        end_date=end_date,
        enabled=enabled,
        description_en=description_en,
        description_nl=description_nl,
    )


def edit_company_job(
    job_id,
    enabled: bool,
    start_date: date,
    end_date: date,
    title_nl: str,
    title_en: str,
    description_nl: str,
    description_en: str,
    contact_name: str,
    contact_email: str,
    contact_address: str,
    contact_city: str,
    website: str,
    phone: str,
    contract_of_service: str,
):
    get_job_by_id(job_id)
    if start_date >= end_date:
        raise ValidationException("Job start date must be before end date")

    company_repository.edit_company_job(
        job_id,
        enabled=enabled,
        start_date=start_date,
        end_date=end_date,
        title_nl=title_nl,
        title_en=title_en,
        description_nl=description_nl,
        description_en=description_en,
        contact_name=contact_name,
        contact_email=contact_email,
        contact_address=contact_address,
        contact_city=contact_city,
        website=website,
        phone=phone,
        contract_of_service=contract_of_service,
    )

    return get_job_by_id(job_id)


def edit_company_banner(
    company_id: int, start_date: date, end_date: date, enabled: bool, website: str
):
    # Check exists
    get_company_banner_by_company_id(company_id)
    if start_date >= end_date:
        raise ValidationException("Job start date must be before end date")

    company_repository.edit_company_banner(
        company_id=company_id,
        start_date=start_date,
        end_date=end_date,
        website=website,
        enabled=enabled,
    )

    return get_company_banner_by_company_id(company_id)


def edit_company_profile(
    company_id: int,
    start_date: date,
    end_date: date,
    enabled: bool,
    description_nl: str,
    description_en: str,
):
    # check exists.
    get_company_profile_by_company_id(company_id)
    if start_date >= end_date:
        raise ValidationException("Profile start date must be before end date")

    company_repository.edit_company_profile(
        company_id=company_id,
        start_date=start_date,
        end_date=end_date,
        enabled=enabled,
        description_en=description_en,
        description_nl=description_nl,
    )

    return get_company_profile_by_company_id(company_id)


def set_logo(company_id: int, logo: FileStorage):
    company = get_company_by_id(company_id)

    if company.logo_file_id:
        old_file = file_service.get_file_by_id(company.logo_file_id)
        company.logo_file_id = None
        file_service.delete_file(old_file)

    new_file = file_service.add_file(FileCategory.COMPANY_LOGO, logo)

    company_repository.set_logo_file_id(company_id=company_id, file_id=new_file.id)


def find_all_jobs(filter_inactive: bool = False) -> list[CompanyJob]:
    return company_repository.find_all_jobs(filter_inactive=filter_inactive)


def find_all_banners(filter_inactive=True) -> list[CompanyBanner]:
    banners = company_repository.find_all_banners(filter_inactive=filter_inactive)

    return banners


def find_all_profiles(filter_inactive=True) -> list[CompanyProfile]:
    profiles = company_repository.find_all_profiles(filter_inactive)

    return profiles


def find_company_jobs_by_company_id(
    company_id: int, filter_inactive: bool = False
) -> list[CompanyJob]:
    return company_repository.find_jobs_by_company_id(
        company_id=company_id, filter_inactive=filter_inactive
    )


def paginated_search_all_jobs(pagination: PageJobSearchParameters):
    return company_repository.paginated_search_all_jobs(
        pagination, filter_inactive=True
    )


def find_all_contract_of_services(filter_inactive: bool = False) -> list[str]:
    return company_repository.find_all_contract_of_services(filter_inactive)


def get_company_banner_by_company_id(company_id: int) -> CompanyBanner:
    banner = company_repository.find_banner_by_company_id(company_id)
    if banner is None:
        raise ResourceNotFoundException("company banner of company", company_id)
    return banner


def get_company_profile_by_company_id(company_id: int) -> CompanyProfile:
    profile = company_repository.find_profile_by_company_id(company_id)
    if profile is None:
        raise ResourceNotFoundException("company profile of company", company_id)
    return profile
