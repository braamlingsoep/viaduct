import logging
from typing import Literal

from authlib.oauth2 import rfc6750, rfc7009, rfc7662
from authlib.oauth2.rfc6749 import grants
from flask import url_for
from oauthlib.common import generate_token

from app.models.oauth.token import OAuthToken
from app.models.user import User
from app.oauth_scopes import Scopes
from app.repository import oauth_repository
from app.service import user_service

_logger = logging.getLogger(__name__)


class AuthorizationCodeGrant(grants.AuthorizationCodeGrant):
    def save_authorization_code(self, code, request):
        client = request.client
        oauth_repository.create_authorization_code(
            code=code,
            client_id=client.client_id,
            redirect_uri=request.redirect_uri,
            scope=request.scope,
            user_id=request.user.get_user_id(),
        )

    def query_authorization_code(self, code, client):
        item = oauth_repository.get_authorization_code_by_client_id_and_code(
            client_id=client.client_id, code=code
        )
        if item and not item.is_expired():
            return item
        return None

    def delete_authorization_code(self, authorization_code):
        oauth_repository.delete_authorization_code(authorization_code)

    def authenticate_user(self, authorization_code):
        return user_service.get_user_by_id(authorization_code.user_id)


class ClientCredentialsGrant(grants.ClientCredentialsGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = ["client_secret_basic", "client_secret_post"]


class RefreshTokenGrant(grants.RefreshTokenGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = ["client_secret_basic", "client_secret_post"]

    def authenticate_refresh_token(self, refresh_token: str) -> OAuthToken | None:
        item = oauth_repository.get_token_by_refresh_token(refresh_token)
        if item and not item.is_refresh_token_expired():
            return item
        return None

    def authenticate_user(self, credential: OAuthToken) -> User | None:
        if not credential.user_id:
            return None
        return user_service.get_user_by_id(credential.user_id)

    def revoke_old_credential(self, credential: OAuthToken) -> None:
        oauth_repository.revoke_token(credential)


class BearerTokenValidator(rfc6750.BearerTokenValidator):
    def authenticate_token(self, token_string):
        return oauth_repository.get_token_by_access_token(token_string)


def _query_token(
    token: str, token_type_hint: Literal["access_token", "refresh_token"] = None
) -> OAuthToken | None:
    if token_type_hint == "access_token":
        return oauth_repository.get_token_by_access_token(access_token=token)
    elif token_type_hint == "refresh_token":
        return oauth_repository.get_token_by_refresh_token(refresh_token=token)

    # Without token_type_hint
    item = oauth_repository.get_token_by_access_token(access_token=token)
    if item:
        return item
    return oauth_repository.get_token_by_refresh_token(refresh_token=token)


class RevocationEndpoint(rfc7009.RevocationEndpoint):
    CLIENT_AUTH_METHODS = ["none", "client_secret_post", "client_secret_basic"]

    def query_token(self, token, token_type_hint):
        return _query_token(token, token_type_hint)

    def revoke_token(self, token, request):
        oauth_repository.revoke_token(token)


class IntrospectionEndpoint(rfc7662.IntrospectionEndpoint):
    CLIENT_AUTH_METHODS = ["none", "client_secret_post", "client_secret_basic"]

    def check_permission(self, token, client, request):
        return True

    def query_token(self, token, token_type_hint):
        return _query_token(token, token_type_hint)

    def introspect_token(self, token: OAuthToken):
        return {
            "active": True,
            "client_id": token.client_id,
            "token_type": token.token_type,
            "username": token.user.email,
            "full_name": token.user.name,
            "scope": token.get_scope(),
            "sub": token.user.id,
            "aud": token.client_id,
            "iss": url_for("home.home", _external=True),
            # TODO Check spec what this should be
            "exp": token.get_expires_in(),
            "iat": token.issued_at,
        }


def get_client_by_id(client_id):
    return oauth_repository.get_client_by_id(client_id)


def create_token(token, request):
    if request.user:
        user_id = request.user.get_user_id()
    else:
        # client_credentials grant_type
        user_id = request.client.user_id

    client_id = request.client.client_id

    return oauth_repository.create_token(client_id=client_id, user_id=user_id, **token)


def get_manual_token(user_id: int) -> OAuthToken:
    client_id = "VIADUCT"
    token = oauth_repository.get_token_by_user_id(user_id, client_id)

    all_scopes = " ".join([scope.name for scope in Scopes])

    if token is not None:
        if (
            not token.is_access_token_expired()
            and token.scope == all_scopes
            and not token.is_revoked()
        ):
            return token
        else:
            oauth_repository.delete_token(token.id)

    token_val = generate_token(42)
    return oauth_repository.create_token(
        client_id=client_id,
        user_id=user_id,
        **{
            "access_token": token_val,
            "expires_in": 86400,
            "refresh_token": None,
            "scope": all_scopes,
            "token_type": "Bearer",
        },
    )


def get_approved_clients_by_user_id(user_id):
    return oauth_repository.get_approved_clients_by_user_id(user_id=user_id)


def user_has_approved_client(user_id, client):
    """Check whether the user has already approved client."""
    return client in oauth_repository.get_approved_clients_by_user_id(user_id)


def get_owned_clients_by_user_id(user_id):
    return oauth_repository.get_owned_clients_by_user_id(user_id=user_id)


def revoke_user_tokens_by_user_id(user_id: int):
    oauth_repository.revoke_user_tokens_by_user_id(user_id)


def revoke_user_tokens_by_client_id(user_id, client_id) -> None:
    oauth_repository.revoke_user_tokens_by_client_id(
        user_id=user_id, client_id=client_id
    )


def get_scope_descriptions():
    return {scope.name: scope.value for scope in Scopes}
