import json
from datetime import datetime
from typing import NamedTuple

import babel.dates as dates
from flask import url_for

from app.models.setting_model import Setting
from app.repository import contracts_repository, setting_repository
from app.utils import zegge


class ContractsSetting(NamedTuple):
    sign_1: str = "Tekenbevoegde 1"
    sign_2: str = "Tekenbevoegde 2"
    contract_field_id: str = ""
    invoice_data_field_id: str = ""
    kvk_field_id: str = ""


def format_datetime(dt) -> str:
    date = datetime.strptime(dt, "%Y-%m-%d")
    return dates.format_datetime(date, "d MMMM YYYY", locale="nl_NL")


class Deal:
    org: dict | None = None

    def __init__(self, data):
        self._data = data

        self.org_name = data.get("org_name", "")
        self.person = data.get("person_name", "")
        self.formatted_value = data.get("formatted_value", "")
        self.products_count = data.get("products_count", "")
        self.name = data.get("title", "")

        self.create_url = url_for("contracts.create_contract", id=data["id"])

    @staticmethod
    def get_pipedrive_field(field_id: str) -> str:
        if not (s := setting_repository.find_by_key("CONTRACTS_SETTINGS")):
            raise Exception("Contract settings must first be set in /admin.")

        data_field_id = json.loads(s.value).get(field_id, "")
        # Fields are 40 characters long
        # See https://pipedrive.readme.io/docs/core-api-concepts-custom-fields
        if len(data_field_id) != 40:
            raise Exception(
                f"Field ID for {field_id} is invalid; is has to be 40 characters long."
            )

        return data_field_id

    @property
    def contract_name(self):
        today = datetime.today()
        return f"{today.strftime('%Y_%m_%d')}_{self.org_name}"

    @property
    def zegge_value(self) -> str:
        return zegge.zegge(self._data["value"])

    @property
    def contract_period(self) -> str | None:
        field = self.get_pipedrive_field("contract_field_id")

        # Check if the field-ids are fields in the response data.
        if field not in self._data.keys() or field + "_until" not in self._data.keys():
            raise Exception("Contract period field ID is invalid.")

        # Check if the fields are set (i.e. not empty).
        if not (start_date := self._data[field]) or not (
            end_date := self._data[field + "_until"]
        ):
            return None

        return f"{format_datetime(start_date)} t/m {format_datetime(end_date)}"

    @property
    def invoice_date(self) -> str | None:
        field = self.get_pipedrive_field("invoice_data_field_id")

        if field not in self._data.keys():
            raise Exception("Invoice date field ID is invalid.")

        if not self._data[field]:
            return None

        return format_datetime(self._data[field])

    @property
    def contract_date(self):
        return dates.format_datetime(datetime.today(), "d MMMM YYYY", locale="nl_NL")

    @property
    def kvk(self) -> str:
        field = self.get_pipedrive_field("kvk_field_id")

        if not self.org:
            raise Exception("Organisation information must be set on the Deal object.")

        if field not in self.org.keys():
            raise Exception("KvK field ID is invalid.")

        return self.org[field]


def get_org_info(org_id: int):
    return contracts_repository.fetch_org_info(org_id)["data"]


def get_single_deal(org_id: int = 0):
    deal_raw = contracts_repository.fetch_single_deal(org_id)
    deal = Deal(deal_raw["data"])

    # Set organisation information; this is only done if a single deal is fetched,
    # since the information is only necessary when creating an actual contract.
    org_id = list(deal_raw["related_objects"]["organization"].values())[0]["id"]
    deal.org = get_org_info(org_id)

    return deal


def get_products_for_deal(org_id: int = 0):
    return contracts_repository.fetch_products_for_deal(org_id)["data"]


def get_all_deals():
    deals = contracts_repository.get_all_deals_raw()["data"]
    return [Deal(d) for d in deals]


def find_settings() -> ContractsSetting:
    if not (s := setting_repository.find_by_key("CONTRACTS_SETTINGS")):
        return ContractsSetting()

    dct = json.loads(s.value)
    return ContractsSetting(
        dct["sign_1"],
        dct["sign_2"],
        dct["contract_field_id"],
        dct["invoice_data_field_id"],
        dct["kvk_field_id"],
    )


def set_settings(settings: ContractsSetting) -> None:
    if not (s := setting_repository.find_by_key("CONTRACTS_SETTINGS")):
        s = Setting()
        s.key = "CONTRACTS_SETTINGS"

    s.value = json.dumps(
        {
            "sign_1": settings.sign_1,
            "sign_2": settings.sign_2,
            "contract_field_id": settings.contract_field_id,
            "invoice_data_field_id": settings.invoice_data_field_id,
            "kvk_field_id": settings.kvk_field_id,
        }
    )

    setting_repository.save(s)
