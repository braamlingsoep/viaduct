from flask import has_request_context
from sqlalchemy import Column, Text

from app import get_locale
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.company import CompanyModuleMixin


@mapper_registry.mapped
class CompanyProfile(BaseEntity, CompanyModuleMixin):
    __tablename__ = "company_profile"
    description_nl = Column(Text)
    description_en = Column(Text)

    @property
    def description(self):
        locale = "en"
        if has_request_context():
            locale = get_locale

        if locale == "nl" and self.description_nl:
            description = self.description_nl
        elif locale == "en" and self.description_en:
            description = self.description_en
        elif self.description_nl:
            description = self.description_nl
        elif self.description_en:
            description = self.description_en
        else:
            description = "N/A"

        return description
