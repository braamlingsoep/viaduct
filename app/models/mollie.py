from typing import Optional

from sqlalchemy import Boolean, Column, Enum, Integer, String
from sqlalchemy.ext.declarative import declared_attr

# TODO declared_attr moves here in SQLA 1.4
from sqlalchemy.orm import (
    declarative_mixin,
    relationship,
)

# , declared_attr
from sqlalchemy.schema import ForeignKey

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.user import User


@mapper_registry.mapped
class Transaction(BaseEntity):
    __tablename__ = "transaction"
    prints = ("id", "mollie_id", "status", "callback_id")

    mollie_id = Column(String(256), unique=True)
    status = Column(
        Enum(
            "open",
            "cancelled",
            "pending",
            "expired",
            "failed",
            "paid",
            "paidout",
            "refunded",
            "charged_back",
            name="transaction_status",
        ),
        nullable=False,
    )
    callback_id = Column(String(256), nullable=False, unique=True)
    has_paid = Column(Boolean(), nullable=False, default=False)

    callback_membership: Optional["TransactionMembership"] = relationship(
        "TransactionMembership", back_populates="transaction", uselist=False
    )

    @classmethod
    def from_mollie_id_status_callback_id(
        cls, mollie_id: str, status: str, callback_id: str
    ):
        m = cls()
        m.mollie_id = mollie_id
        m.status = status
        m.callback_id = callback_id
        m.has_paid = False
        return m


@declarative_mixin
class TransactionCallbackMixin(BaseEntity):
    @declared_attr
    def transaction_id(self) -> "Column[Integer]":
        return Column(Integer, ForeignKey("transaction.id"))


@mapper_registry.mapped
class TransactionMembership(TransactionCallbackMixin):
    __tablename__ = "transaction_membership"

    user: User = relationship("User", uselist=False)
    user_id: int = Column(Integer(), ForeignKey("user.id"), nullable=False)
    transaction: Transaction = relationship(
        "Transaction", back_populates="callback_membership"
    )

    @classmethod
    def from_user(cls, user: User) -> "TransactionMembership":
        m = cls()
        m.user_id = user.id
        return m
