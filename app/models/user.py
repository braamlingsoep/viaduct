from datetime import datetime
from typing import TYPE_CHECKING, Any

from flask_login import AnonymousUserMixin, UserMixin
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    Enum,
    ForeignKey,
    Integer,
    String,
    Text,
    and_,
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped, declarative_mixin, relationship

from app import constants
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.citext import CIText
from app.models.group import Group

if TYPE_CHECKING:
    from app.models.education import Education  # noqa
    from app.models.mailinglist_model import MailingList  # noqa


class AnonymousUser(AnonymousUserMixin):
    """
    Has attributes for flask-login.

    is_anonymous = True, is_active & is_authenticated = False.
    current_user is equal to an instance of this class whenever the user is
    not logged in.

    Check logged in using:
    >>> from flask_login import login_required
    >>> from flask import Blueprint
    >>>
    >>> blueprint = Blueprint("somemodule", __name__)
    >>> @blueprint.route("/someroute")
    >>> @login_required

    Keep in mind, all the user attributes are not available when the user is
    not logged in.
    """

    id = 0
    has_paid = False
    groups: list[Group] = []
    tfa_enabled = False


@mapper_registry.mapped
class UserEducation(BaseEntity):
    __tablename__ = "user_education"
    user_id = Column(Integer, ForeignKey("user.id", ondelete="cascade"), nullable=False)
    education_id: Mapped[int] = Column(
        Integer, ForeignKey("education.id"), nullable=False
    )

    last_seen = Column(DateTime(timezone=True))

    def __repr__(self) -> str:
        return f"<UserEducation {self.modified=} {self.user_id=}, {self.education_id=}>"


@mapper_registry.mapped
class UserEducationStatus(BaseEntity):
    __tablename__ = "user_education_status"

    user_id = Column(Integer, ForeignKey("user.id"), unique=True, nullable=False)
    last_checked = Column(DateTime(timezone=True), nullable=False)


@mapper_registry.mapped
class UserMailinglist:
    __tablename__ = "user_mailinglist"
    user_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="cascade"),
        nullable=False,
        primary_key=True,
    )
    mailinglist_id = Column(
        Integer, ForeignKey("mailing_list.id"), nullable=False, primary_key=True
    )


@mapper_registry.mapped
# TODO https://github.com/sqlalchemy/sqlalchemy/issues/6228
class User(declarative_mixin(UserMixin), BaseEntity):  # type: ignore
    __tablename__ = "user"

    prints = ("id", "email", "password", "first_name", "last_name", "student_id")

    email: str = Column(CIText(), unique=True, nullable=False)
    password = Column(String(60))
    first_name: str = Column(String(256), nullable=False)
    last_name: str = Column(String(256), nullable=False)
    locale: str = Column(
        Enum(*list(constants.LANGUAGES.keys()), name="user_locale"),
        nullable=False,
        default="nl",
    )

    totp_secret = Column(Text, nullable=True)
    tfa_enabled = Column(Boolean, default=False, nullable=True)

    # Only give the user a few minutes to verify secret/otp
    secret_gen_time = Column(DateTime, nullable=True)

    # To prevent replay attack within 30 sec
    last_otp = Column(Text, nullable=True)

    # To prevent brute forcing
    last_login_attempt = Column(DateTime, nullable=True)

    # Membership status
    paid_date = Column(DateTime(timezone=True), nullable=True)
    member_of_merit_date = Column(Date, nullable=True)
    favourer: bool = Column(Boolean, nullable=False, default=False)

    phone_nr = Column(String(16), nullable=False)

    # Study
    student_id: str = Column(String(256), nullable=False)

    birth_date = Column(Date, nullable=True)
    disabled = Column(Boolean, nullable=False, default=False)

    # Location
    address = Column(String(256), nullable=False)
    zip = Column(String(8), nullable=False)
    city = Column(String(256), nullable=False)
    country = Column(String(256), nullable=False, default="Nederland")
    iban = Column(String(32), nullable=True)

    alumnus: bool = Column(Boolean, nullable=False, default=False)

    # (!) There is a education relationship for a read purpose. Do not assign
    # educations using this relationship, but use user_education_service.
    educations: Any = relationship(
        "Education",
        secondary=UserEducation.__table__,
        primaryjoin=lambda: and_(
            User.id == UserEducation.user_id, UserEducation.last_seen.is_(None)
        ),
        lazy="dynamic",
        uselist=True,
        viewonly=True,
    )
    # TODO Remove any: https://github.com/sqlalchemy/sqlalchemy/issues/6229
    mailinglists: Any = relationship(
        "MailingList",
        secondary=UserMailinglist.__table__,
        back_populates="users",
        lazy="dynamic",
        uselist=True,
    )

    copernica_id = Column(Integer(), nullable=True)
    avatar_file_id = Column(Integer, ForeignKey("file.id"))

    student_id_confirmed = Column(Boolean, default=False, nullable=False)

    @hybrid_property
    def has_paid(self) -> bool:
        return self.paid_date is not None

    # We need to ignore the type, as mypy does not understand that
    # @hybrid property is like @property.
    @has_paid.setter  # type: ignore
    def has_paid(self, value: bool) -> None:
        self.paid_date = datetime.now() if value else None

    def __str__(self):
        return self.name

    def get_user_id(self):
        """Retrieve the unique id of the user for authlib."""
        return self.id

    @property
    def name(self):
        if not self.first_name and not self.last_name:
            return None
        return " ".join([self.first_name, self.last_name])

    @property
    def membership_confirmation_pending(self):
        if self.is_anonymous or not self.is_authenticated:
            return False

        return not self.has_paid and not self.alumnus
