from typing import TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, Text
from sqlalchemy.orm import backref, relationship

from app import db
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.activity import Activity
    from app.models.page import Page


@mapper_registry.mapped
class SEO(BaseEntity):
    """Meta tags for SEO of pages, activities and modules."""

    __tablename__ = "seo"

    nl_title = Column(Text)
    en_title = Column(Text)
    nl_description = Column(Text)
    en_description = Column(Text)
    nl_tags = Column(Text)
    en_tags = Column(Text)

    # Necessary page columns.
    page_id = Column(Integer, ForeignKey("page.id"))

    page: "Page" = relationship("Page", backref=backref("seo", lazy="dynamic"))

    activity_id = Column(Integer, ForeignKey("activity.id"))

    activity: "Activity" = relationship(
        "Activity", backref=backref("seo", lazy="dynamic")
    )

    url = Column(Text)

    def __init__(
        self,
        page=None,
        activity=None,
        activity_id=None,
        url=None,
        nl_title="",
        en_title="",
        nl_description="",
        en_description="",
        nl_tags="",
        en_tags="",
    ):
        self.page = page
        self.activity = activity
        self.activity_id = activity_id
        self.url = url

        self.nl_description = nl_description
        self.en_description = en_description
        self.nl_title = nl_title
        self.en_title = en_title
        self.nl_tags = nl_tags
        self.en_tags = en_tags

    @staticmethod
    def get_by_activity(activity_id):
        return db.session.query(SEO).filter(SEO.activity_id == activity_id).first()

    @staticmethod
    def get_by_page(page_id):
        return db.session.query(SEO).filter(SEO.page_id == page_id).first()

    @staticmethod
    def get_by_url(url):
        return db.session.query(SEO).filter(SEO.url == url).first()
