from authlib.integrations.flask_oauth2 import AuthorizationServer
from flask_babel import Babel
from flask_cors import CORS
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import Model, SQLAlchemy
from sqlalchemy import MetaData
from sqlalchemy.orm import registry
from sqlalchemy.orm.decl_api import DeclarativeMeta

cors = CORS()
oauth_server = AuthorizationServer()
babel = Babel()
migrate = Migrate()

# Set up the database.
constraint_naming_convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

# Custom SQLAlchemy object that uses naming conventions.
# https://stackoverflow.com/questions/29153930/
metadata = MetaData(naming_convention=constraint_naming_convention)

# Based on https://docs.sqlalchemy.org/en/14/orm/mapping_styles.html#creating-an-explicit-base-non-dynamically-for-use-with-mypy-similar  # noqa: E501
# This mapper_registry is needed to let mypy correctly detect the models and
# supply type hints for the columns
mapper_registry = registry(metadata=metadata)


class Base(Model, metaclass=DeclarativeMeta):
    __abstract__ = True
    registry = mapper_registry
    metadata = mapper_registry.metadata


db = SQLAlchemy(metadata=metadata, session_options={"future": True})
# Overwrite the Model in flask-sqlalchemy with our base, such that it internally uses
# it correctly.
db.Model = Base

login_manager = LoginManager()
