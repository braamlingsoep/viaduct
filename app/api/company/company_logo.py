from http import HTTPStatus

from flask import Response, request
from flask.views import MethodView
from werkzeug.datastructures import FileStorage

from app.decorators import require_oauth, require_role
from app.exceptions.base import ValidationException
from app.models.company import Company
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import company_service


class CompanyLogoResource(MethodView):
    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    def put(self, company: Company):
        if "file" not in request.files:
            raise ValidationException("No file found.")
        logo: FileStorage = request.files["file"]

        company_service.set_logo(company_id=company.id, logo=logo)
        return Response(status=HTTPStatus.NO_CONTENT)
