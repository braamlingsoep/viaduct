import os
import tempfile
from http import HTTPStatus

from flask import Response, request
from flask.views import MethodView
from marshmallow import fields

from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth
from app.oauth_scopes import Scopes
from app.service import declaration_service


class DeclarationSchema(RestSchema):
    file_data = fields.List(fields.Tuple((fields.String(), fields.String())))
    reason = fields.String()
    committee = fields.String()
    amount = fields.Float()
    iban = fields.String()


class DeclarationResource(MethodView):
    schema = DeclarationSchema()

    @require_oauth(Scopes.declaration)
    @json_schema(schema)
    def post(self, data):
        declaration_service.send_declaration(**data)
        return Response(status=HTTPStatus.NO_CONTENT)


class DeclarationUploadResource(MethodView):
    @require_oauth(Scopes.declaration)
    def post(self):
        try:
            body = request.files
            file_data = []

            for i, _ in enumerate(body):
                file = body["file " + str(i)]
                handle, file_location = tempfile.mkstemp()
                os.close(handle)
                file.save(file_location)
                file_data.append((file_location, file.mimetype))

        except KeyError:
            return Response(HTTPStatus.BAD_REQUEST)

        return {
            "file_data": file_data,
        }, HTTPStatus.OK
