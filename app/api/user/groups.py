import pydantic
from flask import jsonify
from flask.views import MethodView

from app import db
from app.api.group.group import GroupSchema
from app.decorators import require_oauth
from app.models.user import User
from app.oauth_scopes import Scopes
from app.service import group_service
from app.service.group_service import UserGroupMembership


class UserGroupsResource(MethodView):
    schema_get = GroupSchema.get_list_schema()

    @require_oauth(Scopes.user)
    def get(self, user: User):
        groups = group_service.get_groups_for_user(user)
        return self.schema_get.dump(groups)


class UserGroupMembershipsResponse(pydantic.BaseModel):
    groups: list[UserGroupMembership]


class UserGroupDetailsResource(MethodView):
    @require_oauth(Scopes.user)
    def get(self, user: User):
        user_group_memberships = group_service.get_user_group_memberships(
            db.session, user
        )
        return jsonify({"groups": [ugm.dict() for ugm in user_group_memberships]})
