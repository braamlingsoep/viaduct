import typing
from collections.abc import Callable
from typing import Any, TypeVar

from flask_login import current_user
from werkzeug.exceptions import Forbidden, abort
from werkzeug.routing import IntegerConverter, PathConverter

from app.decorators import require_oauth
from app.exceptions.base import ResourceNotFoundException
from app.repository import model_service
from app.roles import Roles

if typing.TYPE_CHECKING:
    from app.models.activity import Activity
    from app.models.news import News

T = TypeVar("T", "Activity", "News")


class UserSelfConverter(IntegerConverter):
    regex = r"(\d+|self)"

    @require_oauth(optional=True)
    def to_python(self, value):
        from app.service import role_service, user_service

        if value == "self":
            if not current_user.is_anonymous:
                return current_user

        if not role_service.user_has_role(current_user, Roles.USER_WRITE):
            raise Forbidden()

        user_id = super().to_python(value)
        return user_service.get_user_by_id(user_id)

    @require_oauth(optional=True)
    def to_url(self, value):
        if current_user == value or value == "self":
            return "self"

        return super().to_url(value.id)


class NavigationEntryConverter(PathConverter):
    def to_python(self, value):
        from app.service import navigation_service

        path = super().to_python(value)
        return navigation_service.get_entry_by_id(path)

    def to_url(self, value):
        return super().to_url(value.id)


class CompanyConverter(PathConverter):
    def to_python(self, value):
        from app.repository import company_repository

        company = company_repository.find_company_by_slug(super().to_python(value))

        if not company:
            raise ResourceNotFoundException("Company by slug", value)

        if not company.active:
            raise abort(410)

        return company

    def to_url(self, value):
        return super().to_url(value.slug)


class PageConverter(IntegerConverter):
    """
    Converts a page_id to a page.

    This also returns pages that have been soft-deleted, unlike the general
    model converter.

    This converter is not used for the user-facing front-end. That uses
    `PagePathConverter`.
    """

    def to_python(self, value):
        from app.service import page_service

        page_id = super().to_python(value)
        return page_service.get_page_by_id(page_id, incl_deleted=True)

    def to_url(self, value):
        return super().to_url(value.id)


class GroupConverter(IntegerConverter):
    def to_python(self, value: Any):
        # Circular imports :(
        from app.service import group_service

        group_id = super().to_python(value)
        return group_service.get_by_id(group_id)

    def to_url(self, value) -> str:
        return super().to_url(value.id)


def create(model_cls):
    class ModelConverter(IntegerConverter):
        def to_python(self, value):
            object_id = super().to_python(value)
            return model_service.get_by_id(model_cls, object_id)

        def to_url(self, value):
            return super().to_url(value.id)

    return ModelConverter


def create_slugable(model_cls: type[T], callable: Callable[[T], str]):
    class SlugableConverter(PathConverter):
        regex = "\\d+/?.*?"

        def to_python(self, value: str) -> T:
            model_id_str, _, _ = value.partition("/")
            model_id = int(model_id_str)
            return model_service.get_by_id(model_cls, model_id)

        def to_url(self, value: T):
            return f"{value.id}/{callable(value)}"

    return SlugableConverter
