from flask_babel import lazy_gettext as _
from flask_wtf import FlaskForm
from wtforms import TextAreaField

from app.forms.util import FieldVerticalSplit


class SeoForm(FlaskForm):
    nl_description = TextAreaField(_("Dutch description (max. 180 characters)"))
    en_description = TextAreaField(_("English description (max. 180 characters)"))
    nl_tags = TextAreaField(_("Dutch tags (comma seperated)"))
    en_tags = TextAreaField(_("English tags (comma seperated)"))

    details = FieldVerticalSplit(
        [
            ["nl_description", "nl_tags"],
            ["en_description", "en_tags"],
        ]
    )
