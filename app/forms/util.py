import inspect
import itertools

from wtforms import DateField, RadioField, SelectFieldBase, SubmitField


class FieldVerticalSplit:
    """
    Vertical field splits.

    Represents a vertical split of fields,
    i.e. fields next to each other.
    """

    def __init__(self, field_names, large_spacing=False):
        """
        field_names should be a list of list of fields to be splitted.

        For example,
            [['X1', 'X2'], ['Y1', 'Y2']]
        will render as:
            [   X1   ]  [   Y1   ]
            [   X2   ]  [   Y2   ]
        """
        self.amount_splits = len(field_names)
        self.type = self.__class__.__name__
        self.form = None
        self.large_spacing = large_spacing
        self._fields = []

        # Allowed amounts of splits which all can be divided evenly
        allowed_split_amounts = [2, 3, 4]

        if self.amount_splits not in allowed_split_amounts:
            raise ValueError(
                "Amount of splits should be equal to one of: {}",
                ", ".join(map(str, allowed_split_amounts)),
            )

        self.field_names_list = field_names

        # Make a list of all fieldnames (i.e. flatten the field_names list)
        self._fieldnames = []
        for fields in self.field_names_list:
            self._fieldnames.extend(fields)

        # First field is used to determine the place of the vertical split
        self._firstfield = field_names[0][0]

        if large_spacing:
            if self.amount_splits == 2:
                self.column_sizes = [5, 5]
            elif self.amount_splits == 3:
                self.column_sizes = [3, 4, 3]
            elif self.amount_splits == 4:
                self.column_sizes = [2, 2, 2, 2]
        else:
            self.column_sizes = [12 // self.amount_splits] * self.amount_splits

    def _set_form(self, form):
        """
        Pass the form to the FieldVerticalSplit.

        Internal method used by FormWrapper.
        """
        self.form = form

        # We have to reset fields here, because it uses global state.
        self._fields = []

        for field_names in self.field_names_list:
            fields = []
            for field_name in field_names:
                fields.append(getattr(form, field_name))
            self._fields.append(fields)

    def __iter__(self):
        if self.form is None:
            raise ValueError("_set_form should be called before iterating")
        return iter(self._fields)

    def __repr__(self):
        return (
            f"<class '{self.type}' form={self.form}, "
            f"fields={self._fields}, "
            f"column_sizes={self.column_sizes})>"
        )


class FormWrapper:
    """Helper class for form rendering."""

    def __init__(self, form):
        self.form = form
        self.vsplits = []
        self.ordered_multiselect_fields = []

        self.csrf_token = form.csrf_token

        self.has_select_fields = False
        self.has_submit_field = False
        self.has_date_fields = False

        for attrname, obj in inspect.getmembers(form):
            # Collect the vertical splits in the form
            if isinstance(obj, FieldVerticalSplit):
                obj.name = attrname
                self.vsplits.append(obj)

            # Check if the form has select fields
            elif isinstance(obj, SelectFieldBase) and not isinstance(obj, RadioField):
                self.has_select_fields = True

            # Check if the form has a submit field
            elif isinstance(obj, SubmitField):
                self.has_submit_field = True

            elif isinstance(obj, DateField):
                self.has_date_fields = True

        try:
            # Dictionary from first field object of a vertial split
            # to the vertical split object itself
            vsplits_firstfields = {
                getattr(form, v._firstfield): v for v in self.vsplits
            }

            # List of all fields belonging to a vertical split
            vsplit_fields = list(
                map(
                    lambda f: getattr(form, f),
                    itertools.chain(*map(lambda v: v._fieldnames, self.vsplits)),
                )
            )
        except TypeError:
            raise TypeError("Vertical split field should be a string")

        self._fields = []

        ignore_fields = []

        if hasattr(form, "_RenderIgnoreFields"):
            ignore_fields = form._RenderIgnoreFields

        for field in form:
            # Add the vertical split when the first field
            # occurs in the field list
            if field in vsplits_firstfields:
                self._fields.append(vsplits_firstfields[field])

            # Otherwise, add a field when it does not belong to a group
            elif field not in vsplit_fields and field.name not in ignore_fields:
                self._fields.append(field)

        # Give every group and vsplit the form object to make them
        # iterable over their tabs/fields
        for g in self.vsplits:
            g._set_form(form)

    def __iter__(self):
        return iter(self._fields)
