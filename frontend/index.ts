import "bootstrap";
import Vue from "vue";
import VueRouter from "vue-router";
import VueCarousel from "vue-carousel";
/*
 * If you want to start using dependencies, please do it the following way:
 * https://bootstrap-vue.js.org/docs/#tree-shaking-with-module-bundlers
 * ~ Wilco - 09-11-2019
 */
import * as Sentry from "@sentry/browser";
import { i18n } from "./translations";
import { Breadcrumb, Form, FormModel, Pagination } from "ant-design-vue";
import "./assets/scss/ant.less";
import { router } from "./router";
import { errorHandler } from "./utils/errors";
import Flask from "./utils/flask";

Vue.use(Form);
Vue.use(FormModel);

/*
 * These are the "misc" imports, they aren't packed together.
 */
const App = () => import("./components/App.vue");
const FooterCarousel = () => import("./components/footer_carousel.vue");
const WtformDatePicker = () =>
    import("./components/form/wtform_date_picker.vue");
const WtformSelectField = () =>
    import("./components/form/wtform_select_field.vue");

declare let SentryConfig: Sentry.BrowserOptions;

Vue.use(VueRouter);
Vue.use(VueCarousel);
Vue.config.errorHandler = errorHandler;
Vue.config.ignoredElements = ["pretix-widget"];

SentryConfig.integrations = [new Sentry.Integrations.Vue()];
Sentry.init(SentryConfig);

// Needed to make sure that 'Sentry' object in globally visible in browser
window["Sentry"] = Sentry;

new Vue({
    el: "#container-main",
    i18n,
    router,
    components: {
        App: App,
        "footer-carousel": FooterCarousel,
        "wtform-date-picker": WtformDatePicker,
        "wtform-select-field": WtformSelectField,
        "a-breadcrumb": Breadcrumb,
        "a-breadcrumb-item": Breadcrumb.Item,
        "a-pagination": Pagination,
    },
    methods: {
        // This method exists to trigger navigation events from
        // Vue components and events to jinja rendered pages.
        // This cannot be an inline function in a template, since
        // the `window` object is not available.
        setLocationHref: (
            page: number,
            endpoint: string,
            kwargs: Record<string, unknown>
        ) => {
            const url = Flask.url_for(endpoint, {
                page: page,
                ...kwargs,
            });
            window.location.href = url;
        },
    },
});
