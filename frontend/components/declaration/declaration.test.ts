import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import Declaration from "./declaration.vue";
import VueRouter from "vue-router";

jest.mock("../../utils/flask.ts");
jest.mock("../../scripts/api/page.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);
import { i18n } from "../../translations";
import flushPromises from "flush-promises";

const consoleSpy = jest.spyOn(console, "error");

describe(Declaration.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(Declaration, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            provide: { locale: "en" },
        });

        await flushPromises();

        getByText("declaration en title");
        getByText("declaration en content");
        getByText("declaration en title");
        getByText("declaration en content");
    });
});
