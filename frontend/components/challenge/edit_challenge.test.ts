import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import EditChallenge from "./edit_challenge.vue";
import VueRouter from "vue-router";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);
import { i18n } from "../../translations";

const consoleSpy = jest.spyOn(console, "error");

describe(EditChallenge.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(EditChallenge, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
        });

        expect(getAllByText("Create challenge")).toHaveLength(2);
    });
});
