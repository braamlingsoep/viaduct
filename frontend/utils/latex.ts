/**
 * Filters commonly occurring LaTeX symbols from a string.
 * @param s
 */
export function filterLatexSymbols(s: string): string {
    // Uses Regex replace because the default replace method only replaces the first occurrence.
    return s
        .replace(/``/g, '"')
        .replace(/`/g, "'")
        .replace(/''/g, '"')
        .replace(/\\\\/g, "")
        .replace(/\\€/g, "€");
}
