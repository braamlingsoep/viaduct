import { Api } from "./api";
import Flask from "../../utils/flask";

export class UploadResponse {
    file_data: [string, string][];
}

export class DeclarationBody {
    file_data: [string, string][];
    reason: string;
    amount: number;
    committee: string;
    iban: string;

    constructor(file_data, reason, amount, committee, iban) {
        this.file_data = file_data;
        this.reason = reason;
        this.amount = amount;
        this.committee = committee;
        this.iban = iban;
    }
}

export class DeclarationOverviewResponse {
    id: number;
    created: string;
    committee: string;
    amount: number;
    reason: string;
}

class DeclarationApi extends Api {
    public async createDeclaration(data: DeclarationBody) {
        return this.post(Flask.url_for("api.declaration.send"), data);
    }

    public async uploadFile(data: FormData) {
        return this.post<UploadResponse>(
            Flask.url_for("api.declaration.upload"),
            data
        );
    }

    public async getDeclarations() {
        return this.get<DeclarationOverviewResponse[]>(
            Flask.url_for("api.user.declarations", { user: "self" })
        );
    }
}

export const declarationApi = new DeclarationApi();
