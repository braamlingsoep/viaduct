import { Api } from "./api";
import Flask from "../../utils/flask";

export interface LectureMailingRequest {
    first_name: string;
    last_name: string;
    email: string;
    student_id: string;
}

class EducationApi extends Api {
    registerLectureMailing(r: LectureMailingRequest) {
        return this.post<unknown>(Flask.url_for("api.lectures.mailinglist"), r);
    }
}

export const educationApi = new EducationApi();
