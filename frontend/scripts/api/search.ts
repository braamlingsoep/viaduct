import { Api } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";

class IndicesResponse {
    indices: string[];
}

class SearchApi extends Api {
    searchIndices(): AxiosPromise<IndicesResponse> {
        return this.get<IndicesResponse>(Flask.url_for("api.search.index", {}));
    }
}

export const searchApi = new SearchApi();
