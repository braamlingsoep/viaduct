import { Api } from "./api";
import { AxiosResponse } from "axios";
import Flask from "../../utils/flask";
import { MultilangString } from "../../types/page";

export class Committee {
    public id: number;
    public created: string;
    public modified: string;
    public name: MultilangString;
    public description: MultilangString;

    public coordinator: any = null;
    public picture_file_id: number | null = null;
    public coordinator_interim: boolean;
    public group: any = null;
    public page: any = null;
    public open_new_members: boolean;
    public tags: CommitteeTag[];
    public pressure: number;
}

export class CommitteeTag {
    public id: number;
    public name: MultilangString;
}

class CommitteeApi extends Api {
    public async getAll(): Promise<AxiosResponse<Committee[]>> {
        return this.get(Flask.url_for("api.committees"));
    }

    public async getCommittee(
        committeeId: number
    ): Promise<AxiosResponse<Committee>> {
        return this.get(
            Flask.url_for("api.committee", { committee: committeeId })
        );
    }

    async createCommittee(values) {
        return this.post<Committee>(
            Flask.url_for("api.committees", {}),
            values
        );
    }

    async updateCommittee(committeeId: number, values) {
        return this.put<Committee>(
            Flask.url_for("api.committee", { committee: committeeId }),
            values
        );
    }

    async deleteCommittee(committeeId: number) {
        return this.delete(
            Flask.url_for("api.committee", { committee: committeeId })
        );
    }

    async setCommitteePicture(committeeId: number, blob: Blob) {
        const formData = new FormData();
        formData.append("file", blob);
        return this.put<void>(
            Flask.url_for("api.committees.picture", { committee: committeeId }),
            formData
        );
    }

    async getAllTags() {
        return this.get<CommitteeTag[]>(Flask.url_for("api.committees.tags"));
    }
}

export const committeeApi = new CommitteeApi();
