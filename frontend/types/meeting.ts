export class Meeting {
    public group_id: number;
    public id: number;
    public name = "";
    public start_time: Date;
    public end_time: Date;
    public location = "";
    public description = "";
    public cancelled = false;
}
